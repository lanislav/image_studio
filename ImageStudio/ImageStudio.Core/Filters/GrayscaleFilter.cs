﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace ImageStudio.Core.Filters
{
    public static class GrayscaleFilter
    {
        /// <summary>
        /// Peforms the grayscale avg. filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <returns>The filtered image.</returns>
        public static Image GrayscaleAvg(Image image)
        {
            return Grayscale(image, GrayscaleAvg);
        }

        /// <summary>
        /// Peforms the grayscale avg. filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <returns>The filtered image.</returns>
        public static Image GrayscaleMax(Image image)
        {
            return Grayscale(image, GrayscaleMax);
        }

        /// <summary>
        /// Peforms the grayscale avg. filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <returns>The filtered image.</returns>
        public static Image GrayscaleMinMaxMean(Image image)
        {
            return Grayscale(image, GrayscaleMinMaxMean);
        }

        /// <summary>
        /// Peforms the grayscale filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <param name="gFunc">The transform function on the pixel.</param>
        /// <returns>The filtered image.</returns>
        internal static Image Grayscale(Image image, Func<Color, Color> gFunc)
        {
            using (var bmp = new Bitmap(image))
            {
                for (var i = 0; i < bmp.Width; i++)
                {
                    for (var j = 0; j < bmp.Height; j++)
                    {
                        bmp.SetPixel(i, j, gFunc(bmp.GetPixel(i, j)));
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Peforms the grayscale avg. filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <returns>The filtered image.</returns>
        public static Image GrayscaleAvgUnsafe(Image image)
        {
            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                var grayscale = GrayscaleAvgUnsafe(p);
                                p[0] = grayscale;
                                p[1] = grayscale;
                                p[2] = grayscale;

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Peforms the grayscale avg. filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <returns>The filtered image.</returns>
        public static Image GrayscaleMaxUnsafe(Image image)
        {
            using (var bmp = new Bitmap(image))
            {
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmpData.Stride;
                var scan0 = bmpData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                var grayscale = GrayscaleMaxUnsafe(p);
                                p[0] = grayscale;
                                p[1] = grayscale;
                                p[2] = grayscale;

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmpData);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Peforms the grayscale avg. filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <returns>The filtered image.</returns>
        public static Image GrayscaleMinMaxMeanUnsafe(Image image)
        {
            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                var grayscale = GrayscaleMinMaxMeanUnsafe(p);
                                p[0] = grayscale;
                                p[1] = grayscale;
                                p[2] = grayscale;

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static Color GrayscaleAvg(Color c)
        {
            var r = c.R;
            var g = c.G;
            var b = c.B;
            var grayscale = (r + g + b) / 3;
            return Color.FromArgb(grayscale, grayscale, grayscale);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static Color GrayscaleMax(Color c)
        {
            var r = c.R;
            var g = c.G;
            var b = c.B;
            var grayscale = Math.Max(r, Math.Max(g, b));
            return Color.FromArgb(grayscale, grayscale, grayscale);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static Color GrayscaleMinMaxMean(Color c)
        {
            var r = c.R;
            var g = c.G;
            var b = c.B;
            var grayscaleMax = Math.Max(r, Math.Max(g, b));
            var grayscaleMin = Math.Min(r, Math.Min(g, b));
            var grayscale = (grayscaleMax + grayscaleMin) / 2;
            return Color.FromArgb(grayscale, grayscale, grayscale);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static unsafe byte GrayscaleAvgUnsafe(byte* p)
        {
            var grayscale = (p[0] + p[1] + p[2]) / 3;
            p[0] = (byte)grayscale;
            p[1] = (byte)grayscale;
            p[2] = (byte)grayscale;

            return (byte) grayscale;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static unsafe byte GrayscaleMaxUnsafe(byte* p)
        {
            var grayscale = Math.Max(p[0], Math.Max(p[1], p[2]));

            return grayscale;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static unsafe byte GrayscaleMinMaxMeanUnsafe(byte* p)
        {
            var grayscale = (Math.Max(p[0], Math.Max(p[1], p[2])) + Math.Min(p[0], Math.Min(p[1], p[2]))) / 2;

            return (byte) grayscale;
        }
    }
}
