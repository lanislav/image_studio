﻿using System.Drawing;
using ImageStudio.Core.Filters;

namespace ImageStudio.Core.Commands
{
    public class CrossDomainColorizeCommand : ICommand
    {
        private readonly Image _image;
        private readonly bool _safeMode;
        private readonly double _hue;
        private readonly double? _saturation;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvertFilterCommand"/> class.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> the command will be executed on.</param>
        /// <param name="newSaturation"></param>
        /// <param name="safeMode">Whether the command runs in safe mode or not.</param>
        /// <param name="newHue"></param>
        public CrossDomainColorizeCommand(Image image, bool safeMode, double newHue, double? newSaturation)
        {
            _image = image;
            _safeMode = safeMode;

            _hue = newHue;
            _saturation = newSaturation;
        }

        /// <summary>
        /// Gets the name property.
        /// </summary>
        public string Name => "Cross Domain Colorize";

        /// <summary>
        /// Executes the filter.
        /// </summary>
        /// <returns>The filtered image.</returns>
        public Image Execute()
        {
            return _safeMode ? CrossDomainColorizeFilter.Colorize(_image, _hue, _saturation) : CrossDomainColorizeFilter.ColorizeUnsafe(_image, _hue, _saturation);
        }

        /// <summary>
        /// Unexecutes the filter.
        /// </summary>
        /// <returns>The original image.</returns>
        public Image UnExecute()
        {
            return _image;
        }
    }
}
