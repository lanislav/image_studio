﻿using System.Drawing;
using ImageStudio.Core.Enums;
using ImageStudio.Core.Stack;

namespace ImageStudio.GUI.Views
{
    /// <summary>
    /// An interface containing the basic properties and methods of a view.
    /// </summary>
    public interface IImageView
    {
        Bitmap Bitmap { get; set; }
        ViewDisplayFormat ViewDisplayFormat { get; set; }
        ConvolutionMatrixSize ConvolutionMatrixSize { get; set; }
        int SaveFileFormatIndex { get; }
        bool IsSafeMode { get; }
        bool IsConvolutionFilterInplace { get; }
        double RedGamma { get; set; }
        double GreenGamma { get; set; }
        double BlueGamma { get; set; }
        int RandomJitterDegree { get; }

        int RedHistogramMin { get; }
        int RedHistogramMax { get; }
        int GreenHistogramMin { get; }
        int GreenHistogramMax { get; }
        int BlueHistogramMin { get; }
        int BlueHistogramMax { get; }

        int KuwaharaSize { get; }

        int PointX { get; set; }
        int PointY { get; set; }
        Color Color { get; }
        double SimilarityTreshold { get; }

        double Hue { get; }
        double? Saturation { get; }

        void TerminateView();
        string LoadImage();
        string SaveImage();
        void InsertUndoItem(string caption);
        void InsertRedoItem(string caption);
        void ClearRedoItems();
        void ClearUndoRedoItems();
        void UndoStackOnReshapeHandler(object sender, StackReshapeEventArgs e);
        void RedoStackOnReshapeHandler(object sender, StackReshapeEventArgs e);
        double GetUndoRedoStackConstraint();
    }
}
