﻿using System.Drawing;
using ImageStudio.Core.Filters;

namespace ImageStudio.Core.Commands
{
    public class HistogramCommand : ICommand
    {
        private readonly Image _image;
        private readonly int _rMin;
        private readonly int _rMax;
        private readonly int _gMin;
        private readonly int _gMax;
        private readonly int _bMin;
        private readonly int _bMax;
        
        public string Name => "Histogram";

        public HistogramCommand(Image image, int rMin, int rMax, int gMin, int gMax, int bMin, int bMax)
        {
            _image = image;
            _rMin = rMin;
            _rMax = rMax;
            _gMin = gMin;
            _gMax = gMax;
            _bMin = bMin;
            _bMax = bMax;
        }

        public Image Execute()
        {
            return HistogramFilter.Histogram(_image, _rMin, _rMax, _gMin, _gMax, _bMin, _bMax);
        }

        public Image UnExecute()
        {
            return _image;
        }
    }
}
