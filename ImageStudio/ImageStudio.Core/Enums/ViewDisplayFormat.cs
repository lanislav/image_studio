﻿namespace ImageStudio.Core.Enums
{
    public enum ViewDisplayFormat
    {
        OriginalImage = 1,
        ChannelImages = 2,
        ConvolutionComparisonImages = 3,
        ChannelHistogram = 4,
        GrayscaleComparisonImages = 5,
        DownsampleImages = 6
    }
}
