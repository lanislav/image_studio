﻿using System.Drawing;
using ImageStudio.Core.Filters;

namespace ImageStudio.Core.Commands
{
    /// <summary>
    /// Implements the <see cref="ICommand"/> interface for the <see cref="EdgeDetectDifferenceFilter"/>.
    /// </summary>
    public class EdgeDetectDifferenceCommand : ICommand
    {
        private readonly Image _image;
        private readonly int _threshold;

        /// <summary>
        /// Initializes a new instance of the <see cref="EdgeDetectDifferenceCommand"/> class.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> the command will be executed on.</param>
        /// <param name="threshold"></param>
        public EdgeDetectDifferenceCommand(Image image, int threshold)
        {
            _image = image;
            _threshold = threshold;
        }

        /// <summary>
        /// Gets the name property.
        /// </summary>
        public string Name => "Edge Detect Difference";

        /// <summary>
        /// Executes the filter.
        /// </summary>
        /// <returns>The filtered image.</returns>
        public Image Execute()
        {
            return EdgeDetectDifferenceFilter.EdgeDetectDifference(_image, _threshold);
        }

        /// <summary>
        /// Unexecutes the filter.
        /// </summary>
        /// <returns>The original image.</returns>
        public Image UnExecute()
        {
            return _image;
        }
    }
}
