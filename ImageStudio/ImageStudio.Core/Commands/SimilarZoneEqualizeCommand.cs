﻿using System.Drawing;
using ImageStudio.Core.Filters;

namespace ImageStudio.Core.Commands
{
    /// <summary>
    /// 
    /// </summary>
    public class SimilarZoneEqualizeCommand : ICommand
    {
        private readonly Image _image;
        private readonly Color _color;
        private readonly int _pointX;
        private readonly int _pointY;
        private readonly double _similarityThreshold;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="color"></param>
        /// <param name="pointX"></param>
        /// <param name="pointY"></param>
        /// <param name="similarityThreshold"></param>
        public SimilarZoneEqualizeCommand(Image image, Color color, int pointX, int pointY, double similarityThreshold)
        {
            _image = image;
            _color = color;
            _pointX = pointX;
            _pointY = pointY;
            _similarityThreshold = similarityThreshold;
        }

        /// <summary>
        /// Gets the name property.
        /// </summary>
        public string Name => "Similar Zone Equalize";

        /// <summary>
        /// Executes the filter.
        /// </summary>
        /// <returns>The filtered image.</returns>
        public Image Execute()
        {
            return SimilarZoneEqualizationFilter.Equalize(_image, _color, _pointX, _pointY, _similarityThreshold);
        }

        /// <summary>
        /// Unexecutes the filter.
        /// </summary>
        /// <returns>The original image.</returns>
        public Image UnExecute()
        {
            return _image;
        }
    }
}
