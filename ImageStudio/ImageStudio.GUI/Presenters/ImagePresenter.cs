﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ImageStudio.Core.AttributeModels;
using ImageStudio.Core.Commands;
using ImageStudio.Core.Enums;
using ImageStudio.Core.Filters;
using ImageStudio.Core.Models;
using ImageStudio.Core.UndoRedo;
using ImageStudio.Core.Utility;
using ImageStudio.GUI.Views;

namespace ImageStudio.GUI.Presenters
{
    public class ImagePresenter
    {
        /// <summary>
        /// The maximum horizontal space available on the display.
        /// </summary>
        private const int HorizontalSpaceAvailableForImage = 860;

        /// <summary>
        /// The maximum vertical space available on the display.
        /// </summary>
        private const int VerticalSpaceAvailableForImage = 600;

        private IImageView _view;
        private ImageContainer _imageContainer;
        private readonly UndoRedo _undoRedo;

        private Rectangle _rectInternal;

        /// <summary>
        /// Gets or sets <see cref="IImageView"/> property.
        /// </summary>
        public IImageView View
        {
            get => _view;
            set => _view = value ?? throw new ArgumentNullException(nameof(value), @"Can not assign a null value to a view.");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImagePresenter"/> class with the specified <see cref="IImageView"/>.
        /// </summary>
        /// <param name="view">The <see cref="IImageView"/> that will be associated to the presenter.</param>
        public ImagePresenter(IImageView view)
        {
            View = view;

            _undoRedo = new UndoRedo(100.0)
            {
                UndoSizeConstrainedStackOnReshape = View.UndoStackOnReshapeHandler,
                RedoSizeConstrainedStackOnReshape = View.RedoStackOnReshapeHandler
            };
        }

        #region Display Logic

        /// <summary>
        /// Closes the associated view.
        /// </summary>
        public void Close()
        {
            View.TerminateView();
        }

        /// <summary>
        /// Clears the <see cref="Image"/> of the associated view.
        /// </summary>
        public void ClearImage()
        {
            _imageContainer.ImageDimensions = new Size(0, 0);

            View.Bitmap = null;
        }

        /// <summary>
        /// Loads a <see cref="Image"/> to the associated view.
        /// </summary>
        public void LoadImage()
        {
            var fileName = View.LoadImage();

            if (string.IsNullOrWhiteSpace(fileName))
            {
                return;
            }

            _undoRedo.Clear();
            View.ClearUndoRedoItems();

            var image = Image.FromFile(fileName);

            _imageContainer.ImageDimensions = image.Size;

            View.Bitmap = new Bitmap(image, image.Size);

            image.Dispose();
        }

        /// <summary>
        /// Saves a <see cref="Image"/> from the associated view.
        /// </summary>
        public void SaveImage()
        {
            var fileName = View.SaveImage();

            if (string.IsNullOrWhiteSpace(fileName))
            {
                return;
            }

            using (var fileStream = File.Open(fileName, FileMode.Create, FileAccess.ReadWrite))
            {
                switch ((SaveImageFormat)View.SaveFileFormatIndex)
                {
                    case SaveImageFormat.Bmp:
                        View.Bitmap.Save(fileStream, ImageFormat.Bmp);
                        break;
                    case SaveImageFormat.Png:
                        View.Bitmap.Save(fileStream, ImageFormat.Png);
                        break;
                    case SaveImageFormat.Jpeg:
                        View.Bitmap.Save(fileStream, ImageFormat.Jpeg);
                        break;
                    default:
                        View.Bitmap.Save(fileStream, View.Bitmap.RawFormat);
                        break;
                }
            }
        }

        /// <summary>
        /// Draws a <see cref="Image"/> to the associated view.
        /// </summary>
        /// <param name="g">The <see cref="Graphics"/> object used to draw the image.</param>
        public void DrawImage(Graphics g)
        {
            switch (View.ViewDisplayFormat)
            {
                case ViewDisplayFormat.OriginalImage:
                    DrawOriginalImage(g);
                    break;
                case ViewDisplayFormat.ChannelImages:
                    DrawChannelImages(g);
                    break;
                case ViewDisplayFormat.ConvolutionComparisonImages:
                    DrawConvolutionComparisonImages(g);
                    break;
                case ViewDisplayFormat.ChannelHistogram:
                    DrawChannelHistogram(g);
                    break;
                case ViewDisplayFormat.GrayscaleComparisonImages:
                    DrawGrayscaleComparisonImages(g);
                    break;
                case ViewDisplayFormat.DownsampleImages:
                    DrawDownsampleImages(g);
                    break;
            }
        }

        /// <summary>
        /// Draws a <see cref="Image"/> to the associated view.
        /// </summary>
        /// <param name="g">The <see cref="Graphics"/> object used to draw the image.</param>
        private void DrawOriginalImage(Graphics g)
        {
            if (g == null || View.Bitmap == null)
            {
                return;
            }

            var img = new Bitmap(View.Bitmap);
            var rect = FitImageToCenterOfContainer(img, HorizontalSpaceAvailableForImage,
                VerticalSpaceAvailableForImage);
            _rectInternal = rect;
            g.DrawImage(img, rect);

            img.Dispose();
            g.Dispose();
        }

        /// <summary>
        /// Draws a <see cref="Image"/> and its channel images to the associated view.
        /// </summary>
        /// <param name="g">The <see cref="Graphics"/> object used to draw the image.</param>
        private void DrawChannelImages(Graphics g)
        {
            if (g == null || View.Bitmap == null)
            {
                return;
            }

            var img = new Bitmap(View.Bitmap);

            var centeredRect =
                FitImageToCenterOfContainer(img, HorizontalSpaceAvailableForImage, VerticalSpaceAvailableForImage);

            if (View.Bitmap.Width > HorizontalSpaceAvailableForImage / 2 ||
                View.Bitmap.Height > VerticalSpaceAvailableForImage / 2)
            {
                img = new Bitmap(img.Resize(centeredRect.Width / 2 - 5, centeredRect.Height / 2 - 5));
            }

            var redImg = View.IsSafeMode ? img.GetRedChannel() : img.GetRedChannelUnsafe();
            var greenImg = View.IsSafeMode ? img.GetGreenChannel() : img.GetGreenChannelUnsafe();
            var blueImg = View.IsSafeMode ? img.GetBlueChannel() : img.GetBlueChannelUnsafe();

            g.DrawImage(img, new Rectangle(centeredRect.Left, centeredRect.Top, img.Width, img.Height));
            g.DrawImage(redImg, new Rectangle(centeredRect.Right - img.Width, centeredRect.Top, img.Width, img.Height));
            g.DrawImage(greenImg, new Rectangle(centeredRect.Left, centeredRect.Bottom - img.Height, img.Width, img.Height));
            g.DrawImage(blueImg, new Rectangle(centeredRect.Right - img.Width, centeredRect.Bottom - img.Height, img.Width, img.Height));

            img.Dispose();
            redImg.Dispose();
            greenImg.Dispose();
            blueImg.Dispose();
            g.Dispose();
        }

        /// <summary>
        /// Draws a <see cref="Image"/> and its channel images to the associated view.
        /// </summary>
        /// <param name="g">The <see cref="Graphics"/> object used to draw the image.</param>
        private void DrawDownsampleImages(Graphics g)
        {
            if (g == null || View.Bitmap == null)
            {
                return;
            }

            var img = new Bitmap(View.Bitmap);

            var centeredRect =
                FitImageToCenterOfContainer(img, HorizontalSpaceAvailableForImage, VerticalSpaceAvailableForImage);

            if (View.Bitmap.Width > HorizontalSpaceAvailableForImage / 2 ||
                View.Bitmap.Height > VerticalSpaceAvailableForImage / 2)
            {
                img = new Bitmap(img.Resize(centeredRect.Width / 2 - 5, centeredRect.Height / 2 - 5));
            }

            g.DrawImage(img, new Rectangle(centeredRect.Left, centeredRect.Top, img.Width, img.Height));
            g.DrawImage(DownsampleFilter.Downsample(img, ImageColorChannel.R), new Rectangle(centeredRect.Right - img.Width, centeredRect.Top, img.Width, img.Height));
            g.DrawImage(DownsampleFilter.Downsample(img, ImageColorChannel.G), new Rectangle(centeredRect.Left, centeredRect.Bottom - img.Height, img.Width, img.Height));
            g.DrawImage(DownsampleFilter.Downsample(img, ImageColorChannel.B), new Rectangle(centeredRect.Right - img.Width, centeredRect.Bottom - img.Height, img.Width, img.Height));

            img.Dispose();
            g.Dispose();
        }

        /// <summary>
        /// Draws a <see cref="Image"/> and its channel images to the associated view.
        /// </summary>
        /// <param name="g">The <see cref="Graphics"/> object used to draw the image.</param>
        private void DrawConvolutionComparisonImages(Graphics g)
        {
            if (g == null || View.Bitmap == null)
            {
                return;
            }

            var img = new Bitmap(View.Bitmap);

            var centeredRect =
                FitImageToCenterOfContainer(img, HorizontalSpaceAvailableForImage, VerticalSpaceAvailableForImage);

            if (View.Bitmap.Width > HorizontalSpaceAvailableForImage / 2 ||
                View.Bitmap.Height > VerticalSpaceAvailableForImage / 2)
            {
                img = new Bitmap(img.Resize(centeredRect.Width / 2 - 5, centeredRect.Height / 2 - 5));
            }

            var command3X3 = new EmbossLaplacianCommand(img, ConvolutionMatrixSize.Conv3X3, View.IsConvolutionFilterInplace);
            var command5X5 = new EmbossLaplacianCommand(img, ConvolutionMatrixSize.Conv5X5, View.IsConvolutionFilterInplace);
            var command7X7 = new EmbossLaplacianCommand(img, ConvolutionMatrixSize.Conv7X7, View.IsConvolutionFilterInplace);

            var img3X3 = command3X3.Execute();
            var img5X5 = command5X5.Execute();
            var img7X7 = command7X7.Execute();

            g.DrawImage(img, new Rectangle(centeredRect.Left, centeredRect.Top, img.Width, img.Height));
            g.DrawImage(img3X3, new Rectangle(centeredRect.Right - img.Width, centeredRect.Top, img.Width, img.Height));
            g.DrawImage(img5X5, new Rectangle(centeredRect.Left, centeredRect.Bottom - img.Height, img.Width, img.Height));
            g.DrawImage(img7X7, new Rectangle(centeredRect.Right - img.Width, centeredRect.Bottom - img.Height, img.Width, img.Height));

            img.Dispose();
            img3X3.Dispose();
            img5X5.Dispose();
            img7X7.Dispose();
            g.Dispose();
        }

        /// <summary>
        /// Draws a <see cref="Image"/> and its channel images to the associated view.
        /// </summary>
        /// <param name="g">The <see cref="Graphics"/> object used to draw the image.</param>
        private void DrawChannelHistogram(Graphics g)
        {
            if (g == null || View.Bitmap == null)
            {
                return;
            }

            var img = new Bitmap(View.Bitmap);

            var centeredRect =
                FitImageToCenterOfContainer(img, HorizontalSpaceAvailableForImage, VerticalSpaceAvailableForImage);

            if (View.Bitmap.Width > HorizontalSpaceAvailableForImage / 2 ||
                View.Bitmap.Height > VerticalSpaceAvailableForImage / 2)
            {
                img = new Bitmap(img.Resize(centeredRect.Width / 2 - 5, centeredRect.Height / 2 - 5));
            }

            var histogram = ChannelHistogram.GetChannelHistogramCmy(img);
            
            g.DrawImage(img, new Rectangle(centeredRect.Left, centeredRect.Top, img.Width, img.Height));
            
            ChannelHistogram.DrawHistogram(histogram.RedHistogram, g,
                new Rectangle(centeredRect.Right - img.Width, centeredRect.Top, img.Width, img.Height),
                ImageColorChannel.R);
            ChannelHistogram.DrawHistogram(histogram.GreenHistogram, g,
                new Rectangle(centeredRect.Left, centeredRect.Bottom - img.Height, img.Width, img.Height),
                ImageColorChannel.G);
            ChannelHistogram.DrawHistogram(histogram.BlueHistogram, g,
                new Rectangle(centeredRect.Right - img.Width, centeredRect.Bottom - img.Height, img.Width, img.Height),
                ImageColorChannel.B);

            img.Dispose();
            g.Dispose();
        }

        /// <summary>
        /// Draws a <see cref="Image"/> and its channel images to the associated view.
        /// </summary>
        /// <param name="g">The <see cref="Graphics"/> object used to draw the image.</param>
        private void DrawGrayscaleComparisonImages(Graphics g)
        {
            if (g == null || View.Bitmap == null)
            {
                return;
            }

            var img = new Bitmap(View.Bitmap);

            var centeredRect =
                FitImageToCenterOfContainer(img, HorizontalSpaceAvailableForImage, VerticalSpaceAvailableForImage);

            if (View.Bitmap.Width > HorizontalSpaceAvailableForImage / 2 ||
                View.Bitmap.Height > VerticalSpaceAvailableForImage / 2)
            {
                img = new Bitmap(img.Resize(centeredRect.Width / 2 - 5, centeredRect.Height / 2 - 5));
            }

            var grayscaleAvg = new GrayscaleAvgCommand(img, View.IsSafeMode).Execute();
            var grayscaleMax = new GrayscaleMaxCommand(img, View.IsSafeMode).Execute();
            var grayscaleMinMaxMean = new GrayscaleMinMaxMeanCommand(img, View.IsSafeMode).Execute();

            g.DrawImage(img, new Rectangle(centeredRect.Left, centeredRect.Top, img.Width, img.Height));
            g.DrawImage(grayscaleAvg, new Rectangle(centeredRect.Right - img.Width, centeredRect.Top, img.Width, img.Height));
            g.DrawImage(grayscaleMax, new Rectangle(centeredRect.Left, centeredRect.Bottom - img.Height, img.Width, img.Height));
            g.DrawImage(grayscaleMinMaxMean, new Rectangle(centeredRect.Right - img.Width, centeredRect.Bottom - img.Height, img.Width, img.Height));

            img.Dispose();
            grayscaleAvg.Dispose();
            grayscaleMax.Dispose();
            grayscaleMinMaxMean.Dispose();
            g.Dispose();
        }

        /// <summary>
        /// Fits the specified <see cref="Image"/> to the center of the internal container object.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being fit to the container.</param>
        /// <param name="width">The available width for the <see cref="Image"/>.</param>
        /// <param name="height">The available height for the <see cref="Image"/>.</param>
        /// <returns>A <see cref="Rectangle"/> that is center to the container and fits the <see cref="Image"/>.</returns>
        private Rectangle FitImageToCenterOfContainer(Image image, int width, int height)
        {
            var oldSize = _imageContainer.ImageDimensions;
            if (image.Width > width || image.Height > height)
            {
                var tempImg = new Bitmap(image.Resize(width, height));

                _imageContainer.ImageDimensions = tempImg.Size;

                tempImg.Dispose();
            }

            var result = _imageContainer.FitImageToCenterOfContainer();

            _imageContainer.ImageDimensions = oldSize;

            return result;
        }

        /// <summary>
        /// Calculates the properties of the internal <see cref="ImageContainer"/> object.
        /// </summary>
        /// <param name="width">The width of the container.</param>
        /// <param name="height">The height of the container.</param>
        public void CalculateImageContainer(int width, int height)
        {
            var horizontalMargin = (width - HorizontalSpaceAvailableForImage) / 2;
            var verticalMargin = (height - VerticalSpaceAvailableForImage) / 2;

            _imageContainer = new ImageContainer(HorizontalSpaceAvailableForImage, VerticalSpaceAvailableForImage)
            {
                Margin = new Margin(horizontalMargin, verticalMargin)
            };
        }

        public bool IsWithinBoundsOfImage(int pointX, int pointY)
        {
            if (View.ViewDisplayFormat != ViewDisplayFormat.OriginalImage)
            {
                throw new Exception("Wrong display format!");
            }

            return _rectInternal.Contains(pointX, pointY);
        }

        #endregion

        #region Filters Logic  

        /// <summary>
        /// Inverts the colors of the <see cref="Image"/> associated with the <see cref="IImageView"/>.
        /// </summary>
        public void Invert()
        {
            var command = new InvertFilterCommand(View.Bitmap, View.IsSafeMode);

            ExecuteCommand(command);
        }

        /// <summary>
        /// Peforms the gamma filter on the <see cref="Image"/> associated with the <see cref="IImageView"/>.
        /// </summary>
        public void Gamma()
        {
            var command = new GammaCommand(View.Bitmap, View.IsSafeMode, View.RedGamma, View.GreenGamma,
                View.BlueGamma);

            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void EmbossLaplacian()
        {
            var command = new EmbossLaplacianCommand(View.Bitmap, View.ConvolutionMatrixSize, View.IsConvolutionFilterInplace);

            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void EdgeDetectDifference()
        {
            var command = new EdgeDetectDifferenceCommand(View.Bitmap, 127);

            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void RandomJitter()
        {
            var command = new RandomJitterCommand(View.Bitmap, View.RandomJitterDegree);

            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void GrayscaleAvg()
        {
            var command = new GrayscaleAvgCommand(View.Bitmap, View.IsSafeMode);

            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void GrayscaleMax()
        {
            var command = new GrayscaleMaxCommand(View.Bitmap, View.IsSafeMode);

            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void GrayscaleMinMaxMean()
        {
            var command = new GrayscaleMinMaxMeanCommand(View.Bitmap, View.IsSafeMode);

            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Histogram()
        {
            var command = new HistogramCommand(View.Bitmap, View.RedHistogramMin, View.RedHistogramMax, View.GreenHistogramMin, View.GreenHistogramMax, View.BlueHistogramMin, View.BlueHistogramMax);
            
            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void OrderedDither()
        {
            var command = new OrderedDitherCommand(View.Bitmap, View.IsSafeMode);

            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void StuckiDither()
        {
            var command = new StuckiDitherCommand(View.Bitmap, View.IsSafeMode);

            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SimpleColorize()
        {
            var command = new SimpleColorizeCommand(View.Bitmap, View.IsSafeMode);

            ExecuteCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SimpleColorizeSimilar()
        {
            var fileName = View.LoadImage();

            if (string.IsNullOrWhiteSpace(fileName))
            {
                return;
            }

            var image = Image.FromFile(fileName);

            var command = new SimpleColorizeCommand(View.Bitmap, View.IsSafeMode, image);

            ExecuteCommand(command);
        }

        public void CrossDomainColoize()
        {
            var command = new CrossDomainColorizeCommand(View.Bitmap, View.IsSafeMode, View.Hue, View.Saturation);

            ExecuteCommand(command);
        }

        public void Kuwahara()
        {
            var command = new KuwaharaCommand(View.Bitmap, View.IsSafeMode, View.KuwaharaSize);

            ExecuteCommand(command);
        }

        public void Equalize()
        {
            var command = new SimilarZoneEqualizeCommand(View.Bitmap, View.Color,
                View.PointX, View.PointY, View.SimilarityTreshold);

            ExecuteCommand(command);
        }

        /// <summary>
        /// Pushes the current views <see cref="Image"/> to the undo stack, and executes the specified <see cref="ICommand"/> object.
        /// </summary>
        /// <param name="command">The command that will be executed.</param>
        private void ExecuteCommand(ICommand command)
        {
            if (_undoRedo.InsertCommand(command))
            {
                View.InsertUndoItem(command.Name);
                View.ClearRedoItems();
            }

            View.Bitmap = new Bitmap(command.Execute());
        }

        #endregion

        #region UndoRedo Logic
        
        /// <summary>
        /// Performs the undo operation to the specified depth. Returns the <see cref="Image"/> of the last <see cref="ICommand"/>.
        /// </summary>
        /// <param name="level">The depth of the undo operation.</param>
        public void Undo(int level)
        {
            var image = new Bitmap(_undoRedo.Undo(level));

            View.Bitmap = new Bitmap(image);

            image.Dispose();
        }

        /// <summary>
        /// Performs the redo operation to the specified depth. Returns the <see cref="Image"/> of the last <see cref="ICommand"/>.
        /// </summary>
        /// <param name="level">The depth of the redo operation.</param>
        public void Redo(int level)
        {
            var image = new Bitmap(_undoRedo.Redo(level));

            View.Bitmap = new Bitmap(image);

            image.Dispose();
        }

        /// <summary>
        /// Sets the internal <see cref="UndoRedo"/> objects stack size constraint. 
        /// </summary>
        public void SetUndoRedoStackConstraint()
        {
            var constraint = View.GetUndoRedoStackConstraint();

            if (constraint < 0.0)
            {
                return;
            }

            _undoRedo.SetUndoRedoStackConstraint(constraint);
        }

        #endregion
    }
}
