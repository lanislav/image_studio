﻿using System.Drawing;
using System.Drawing.Imaging;

namespace ImageStudio.Core.Filters
{
    public static class OrderedDitheringFilter
    {
        private static readonly int[][] OrderedDitheringMatrix = {
            new [] { 28, 255, 57 },
            new [] { 142, 113, 227},
            new [] {170, 198, 85}
        };

        private const int OrderedDitheringMatrixSize = 3;

        public static Image Dither(Image image)
        {
            using (var bmp = new Bitmap(image))
            {
                int xCoord, yCoord, val;
                for (var i = 0; i < bmp.Width; i++)
                {
                    for (var j = 0; j < bmp.Height; j++)
                    {
                        xCoord = i % OrderedDitheringMatrixSize;
                        yCoord = j % OrderedDitheringMatrixSize;
                        val = OrderedDitheringMatrix[xCoord][yCoord];

                        var pixel = bmp.GetPixel(i, j);
                        var r = pixel.R > val ? (byte) 255 : (byte) 0;
                        var g = pixel.G > val ? (byte)255 : (byte)0;
                        var b = pixel.B > val ? (byte)255 : (byte)0;

                        bmp.SetPixel(i, j, Color.FromArgb(r, g, b));
                    }
                }

                return new Bitmap(bmp);
            }
        }

        public static Image DitherUnsafe(Image image)
        {
            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;
                        int xCoord, yCoord, val;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                xCoord = x % OrderedDitheringMatrixSize;
                                yCoord = x % OrderedDitheringMatrixSize;
                                val = OrderedDitheringMatrix[xCoord][yCoord];
                                p[0] = p[0] > val ? (byte)255 : (byte)0;
                                p[1] = p[1] > val ? (byte)255 : (byte)0;
                                p[2] = p[2] > val ? (byte)255 : (byte)0;

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }

                return new Bitmap(bmp);
            }
        }
    }
}
