﻿using System.Drawing;
using System.Drawing.Imaging;

namespace ImageStudio.Core.Filters
{
    /// <summary>
    /// Implements the offset filter.
    /// </summary>
    internal static class OffsetFilter
    {
        /// <summary>
        /// Performs the offset filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> being filtered.</param>
        /// <param name="offset">The matrix with offset values.</param>
        /// <returns>The filtered image.</returns>
        public static Image Offset(Image image, Point[,] offset)
        {
            using (var bmp = new Bitmap(image))
            using (var bmpCloned = (Bitmap) bmp.Clone())
            {
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                var bmpClonedData = bmpCloned.LockBits(new Rectangle(0, 0, bmpCloned.Width, bmpCloned.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var scanline = bmpData.Stride;

                var bmpScan0 = bmpData.Scan0;
                var bmpClonedScan0 = bmpClonedData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)bmpScan0;
                        var pCloned = (byte*)(void*)bmpClonedScan0;

                        var nOffset = bmpData.Stride - bmp.Width * 3;
                        var width = bmp.Width;
                        var height = bmp.Height;

                        int xOffset, yOffset;

                        for (var y = 0; y < height; ++y)
                        {
                            for (var x = 0; x < width; ++x)
                            {
                                xOffset = offset[x, y].X;
                                yOffset = offset[x, y].Y;

                                p[0] = pCloned[(y + yOffset) * scanline + (x + xOffset) * 3];
                                p[1] = pCloned[(y + yOffset) * scanline + (x + xOffset) * 3 + 1];
                                p[2] = pCloned[(y + yOffset) * scanline + (x + xOffset) * 3 + 2];

                                p += 3;
                            }
                            p += nOffset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmpData);
                        bmpCloned.UnlockBits(bmpClonedData);
                    }
                }
                
                return new Bitmap(bmp);
            }
        }
    }
}
