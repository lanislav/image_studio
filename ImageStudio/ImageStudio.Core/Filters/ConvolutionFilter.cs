﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using ImageStudio.Core.Models;
using ImageStudio.Core.Utility;

namespace ImageStudio.Core.Filters
{
    /// <summary>
    /// Implements the convolution filter.
    /// </summary>
    internal static class ConvolutionFilter
    {
        private const int Padding3X3 = 2;
        private const int Padding5X5 = 4;
        private const int Padding7X7 = 6;

        /// <summary>
        /// Peforms the convolution filter operation on the specified <see cref="Image"/>, with a 3x3 size <see cref="ConvolutionMatrix"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <param name="m">The <see cref="ConvolutionMatrix"/> object of the filter.</param>
        /// <param name="inplace">Whether the filter is applied inplace or not.</param>
        /// <returns>The filtered image.</returns>
        public static Image Convolution3X3(Image image, ConvolutionMatrix m, bool inplace = false)
        {
            if (m.Factor == 0)
            {
                throw new DivideByZeroException("Attempted to divide conv. matrix by zero.");
            }

            using (var bmp = new Bitmap(image))
            using (var bmpPadded = new Bitmap(image.Pad(Padding3X3)))
            {
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                var bmpPaddedData = bmpPadded.LockBits(new Rectangle(0, 0, bmpPadded.Width, bmpPadded.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var bmpStride = bmpData.Stride;
                var bmpPaddedStride = bmpPaddedData.Stride;

                var bmpScan0 = bmpData.Scan0;
                var bmpPaddedScan0 = bmpPaddedData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)bmpScan0;
                        var pPadded = (byte*)(void*)bmpPaddedScan0;

                        var pixelSize = 3;

                        var offset = bmpStride - bmp.Width * pixelSize;
                        var offsetPadded = bmpPaddedStride - bmpPadded.Width * pixelSize;

                        var width = bmp.Width;
                        var height = bmp.Height;

                        for (var y = 0; y < height; ++y)
                        {
                            for (var x = 0; x < width; ++x)
                            {
                                p[2] = CalculatePixelValue3X3(pPadded, 2, bmpPaddedStride, m);
                                p[1] = CalculatePixelValue3X3(pPadded, 1, bmpPaddedStride, m);
                                p[0] = CalculatePixelValue3X3(pPadded, 0, bmpPaddedStride, m);

                                if (inplace)
                                {
                                    pPadded[2] = p[2];
                                    pPadded[1] = p[1];
                                    pPadded[0] = p[0];
                                }

                                p += pixelSize;
                                pPadded += pixelSize;
                            }

                            p += offset;
                            pPadded += offsetPadded + Padding3X3 * pixelSize;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmpData);
                        bmpPadded.UnlockBits(bmpPaddedData);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Peforms the convolution filter operation on the specified <see cref="Image"/>, with a 5x5 size <see cref="ConvolutionMatrix"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <param name="m">The <see cref="ConvolutionMatrix"/> object of the filter.</param>
        /// <param name="inplace">Whether the filter is applied inplace or not.</param>
        /// <returns>The filtered image.</returns>
        public static Image Convolution5X5(Image image, ConvolutionMatrix m, bool inplace = false)
        {
            if (m.Factor == 0)
            {
                throw new DivideByZeroException("Attempted to divide conv. matrix by zero.");
            }

            using (var bmp = new Bitmap(image))
            using (var bmpPadded = new Bitmap(image.Pad(Padding5X5)))
            {
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                var bmpPaddedData = bmpPadded.LockBits(new Rectangle(0, 0, bmpPadded.Width, bmpPadded.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var bmpStride = bmpData.Stride;
                var bmpPaddedStride = bmpPaddedData.Stride;

                var bmpScan0 = bmpData.Scan0;
                var bmpPaddedScan0 = bmpPaddedData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)bmpScan0;
                        var pPadded = (byte*)(void*)bmpPaddedScan0;

                        var pixelSize = 3;

                        var offset = bmpStride - bmp.Width * pixelSize;
                        var offsetPadded = bmpPaddedStride - bmpPadded.Width * pixelSize;

                        var width = bmp.Width;
                        var height = bmp.Height;

                        for (var y = 0; y < height; ++y)
                        {
                            for (var x = 0; x < width; ++x)
                            {
                                p[2] = CalculatePixelValue5X5(pPadded, 2, bmpPaddedStride, m);
                                p[1] = CalculatePixelValue5X5(pPadded, 1, bmpPaddedStride, m);
                                p[0] = CalculatePixelValue5X5(pPadded, 0, bmpPaddedStride, m);

                                if (inplace)
                                {
                                    pPadded[2] = p[2];
                                    pPadded[1] = p[1];
                                    pPadded[0] = p[0];
                                }

                                p += pixelSize;
                                pPadded += pixelSize;
                            }

                            p += offset;
                            pPadded += offsetPadded + Padding5X5 * pixelSize;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmpData);
                        bmpPadded.UnlockBits(bmpPaddedData);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Peforms the convolution filter operation on the specified <see cref="Image"/>, with a 7x7 size <see cref="ConvolutionMatrix"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <param name="m">The <see cref="ConvolutionMatrix"/> object of the filter.</param>
        /// <param name="inplace">Whether the filter is applied inplace or not.</param>
        /// <returns>The filtered image.</returns>
        public static Image Convolution7X7(Image image, ConvolutionMatrix m, bool inplace = false)
        {
            if (m.Factor == 0)
            {
                throw new DivideByZeroException("Attempted to divide conv. matrix by zero.");
            }

            using (var bmp = new Bitmap(image))
            using (var bmpPadded = new Bitmap(image.Pad(Padding7X7)))
            {
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                var bmpPaddedData = bmpPadded.LockBits(new Rectangle(0, 0, bmpPadded.Width, bmpPadded.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var bmpStride = bmpData.Stride;
                var bmpPaddedStride = bmpPaddedData.Stride;

                var bmpScan0 = bmpData.Scan0;
                var bmpPaddedScan0 = bmpPaddedData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)bmpScan0;
                        var pPadded = (byte*)(void*)bmpPaddedScan0;

                        var pixelSize = 3;

                        var offset = bmpStride - bmp.Width * pixelSize;
                        var offsetPadded = bmpPaddedStride - bmpPadded.Width * pixelSize;

                        var width = bmp.Width;
                        var height = bmp.Height;

                        for (var y = 0; y < height; ++y)
                        {
                            for (var x = 0; x < width; ++x)
                            {
                                p[2] = CalculatePixelValue7X7(pPadded, 2, bmpPaddedStride, m);
                                p[1] = CalculatePixelValue7X7(pPadded, 1, bmpPaddedStride, m);
                                p[0] = CalculatePixelValue7X7(pPadded, 0, bmpPaddedStride, m);

                                if (inplace)
                                {
                                    pPadded[2] = p[2];
                                    pPadded[1] = p[1];
                                    pPadded[0] = p[0];
                                }

                                p += pixelSize;
                                pPadded += pixelSize;
                            }

                            p += offset;
                            pPadded += offsetPadded + Padding7X7 * pixelSize;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmpData);
                        bmpPadded.UnlockBits(bmpPaddedData);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Calculates the pixel value using the specified <see cref="ConvolutionMatrix"/>.
        /// </summary>
        /// <param name="pixelPtr">The pointer to the pixel.</param>
        /// <param name="offset">The offset of the pixel.</param>
        /// <param name="stride">The stride of the image.</param>
        /// <param name="m">The <see cref="ConvolutionMatrix"/> object.</param>
        /// <returns>The calculated pixel value.</returns>
        private static unsafe byte CalculatePixelValue3X3(byte* pixelPtr, int offset, int stride, ConvolutionMatrix m)
        {
            var doubleStride = stride * 2;

            var pixel = (pixelPtr[offset] * m.TopLeft +
                         pixelPtr[offset + 3] * m.TopMid +
                         pixelPtr[offset + 6] * m.TopRight +
                         pixelPtr[offset + stride] * m.MidLeft +
                         pixelPtr[offset + stride + 3] * m.Pixel +
                         pixelPtr[offset + stride + 6] * m.MidRight +
                         pixelPtr[offset + doubleStride] * m.BottomLeft +
                         pixelPtr[offset + doubleStride + 3] * m.BottomMid +
                         pixelPtr[offset + doubleStride + 6] * m.BottomRight)
                        / m.Factor + m.Offset;

            pixel = ClampPixel(pixel);

            return (byte)pixel;
        }

        /// <summary>
        /// Calculates the pixel value using the specified <see cref="ConvolutionMatrix"/>.
        /// </summary>
        /// <param name="pixelPtr">The pointer to the pixel.</param>
        /// <param name="offset">The offset of the pixel.</param>
        /// <param name="stride">The stride of the image.</param>
        /// <param name="m">The <see cref="ConvolutionMatrix"/> object.</param>
        /// <returns>The calculated pixel value.</returns>
        private static unsafe byte CalculatePixelValue5X5(byte* pixelPtr, int offset, int stride, ConvolutionMatrix m)
        {
            var doubleStride = stride * 2;
            var tripleStride = stride * 3;
            var quadrupleStride = stride * 4;

            var pixel = (pixelPtr[offset] * m.TopLeft +
                         pixelPtr[offset + 3] * m.TopLeft +
                         pixelPtr[offset + 6] * m.TopMid +
                         pixelPtr[offset + 9] * m.TopMid +
                         pixelPtr[offset + 12] * m.TopRight +

                         pixelPtr[offset + stride] * m.MidLeft +
                         pixelPtr[offset + stride + 3] * m.TopLeft +
                         pixelPtr[offset + stride + 6] * m.TopMid +
                         pixelPtr[offset + stride + 9] * m.TopRight +
                         pixelPtr[offset + stride + 12] * m.TopRight +

                         pixelPtr[offset + doubleStride] * m.MidLeft +
                         pixelPtr[offset + doubleStride + 3] * m.MidLeft +
                         pixelPtr[offset + doubleStride + 6] * m.Pixel +
                         pixelPtr[offset + doubleStride + 9] * m.MidRight +
                         pixelPtr[offset + doubleStride + 12] * m.MidRight +

                         pixelPtr[offset + tripleStride] * m.BottomLeft +
                         pixelPtr[offset + tripleStride + 3] * m.BottomLeft +
                         pixelPtr[offset + tripleStride + 6] * m.BottomMid +
                         pixelPtr[offset + tripleStride + 9] * m.BottomRight +
                         pixelPtr[offset + tripleStride + 12] * m.MidRight + 

                         pixelPtr[offset + quadrupleStride] * m.BottomLeft +
                         pixelPtr[offset + quadrupleStride + 3] * m.BottomMid +
                         pixelPtr[offset + quadrupleStride + 6] * m.BottomMid +
                         pixelPtr[offset + quadrupleStride + 9] * m.BottomRight +
                         pixelPtr[offset + quadrupleStride + 12] * m.BottomRight)
                        / m.Factor + m.Offset;

            pixel = ClampPixel(pixel);

            return (byte)pixel;
        }

        /// <summary>
        /// Calculates the pixel value using the specified <see cref="ConvolutionMatrix"/>.
        /// </summary>
        /// <param name="pixelPtr">The pointer to the pixel.</param>
        /// <param name="offset">The offset of the pixel.</param>
        /// <param name="stride">The stride of the image.</param>
        /// <param name="m">The <see cref="ConvolutionMatrix"/> object.</param>
        /// <returns>The calculated pixel value.</returns>
        private static unsafe byte CalculatePixelValue7X7(byte* pixelPtr, int offset, int stride, ConvolutionMatrix m)
        {
            var doubleStride = stride * 2;
            var tripleStride = stride * 3;
            var quadrupleStride = stride * 4;
            var pentupleStride = stride * 5;
            var sextupleStride = stride * 6;

            var pixel = (pixelPtr[offset] * m.TopLeft +
                         pixelPtr[offset + 3] * m.TopLeft +
                         pixelPtr[offset + 9] * m.TopMid +
                         pixelPtr[offset + 12] * m.TopMid +
                         pixelPtr[offset + 18] * m.TopRight +

                         pixelPtr[offset + stride + 3] * m.TopLeft +
                         pixelPtr[offset + stride + 6] * m.TopLeft +
                         pixelPtr[offset + stride + 9] * m.TopMid +
                         pixelPtr[offset + stride + 12] * m.TopMid +
                         pixelPtr[offset + stride + 15] * m.TopRight +
                         pixelPtr[offset + stride + 18] * m.TopRight +

                         pixelPtr[offset + doubleStride] * m.MidLeft +
                         pixelPtr[offset + doubleStride + 3] * m.MidLeft +
                         pixelPtr[offset + doubleStride + 6] * m.TopLeft +
                         pixelPtr[offset + doubleStride + 9] * m.TopMid +
                         pixelPtr[offset + doubleStride + 12] * m.TopRight +
                         pixelPtr[offset + doubleStride + 15] * m.TopRight +

                         pixelPtr[offset + tripleStride] * m.MidLeft +
                         pixelPtr[offset + tripleStride + 3] * m.MidLeft +
                         pixelPtr[offset + tripleStride + 6] * m.MidLeft +
                         pixelPtr[offset + tripleStride + 9] * m.Pixel +
                         pixelPtr[offset + tripleStride + 12] * m.MidRight +
                         pixelPtr[offset + tripleStride + 15] * m.MidRight +
                         pixelPtr[offset + tripleStride + 18] * m.MidRight +

                         pixelPtr[offset + quadrupleStride + 3] * m.BottomLeft +
                         pixelPtr[offset + quadrupleStride + 6] * m.BottomLeft +
                         pixelPtr[offset + quadrupleStride + 9] * m.BottomMid +
                         pixelPtr[offset + quadrupleStride + 12] * m.BottomRight +
                         pixelPtr[offset + quadrupleStride + 15] * m.MidRight +
                         pixelPtr[offset + quadrupleStride + 18] * m.MidRight +

                         pixelPtr[offset + pentupleStride] * m.BottomLeft +
                         pixelPtr[offset + pentupleStride + 3] * m.BottomLeft +
                         pixelPtr[offset + pentupleStride + 6] * m.BottomMid +
                         pixelPtr[offset + pentupleStride + 9] * m.BottomMid +
                         pixelPtr[offset + pentupleStride + 12] * m.BottomRight +
                         pixelPtr[offset + pentupleStride + 15] * m.BottomRight +

                         pixelPtr[offset + sextupleStride] * m.BottomLeft +
                         pixelPtr[offset + sextupleStride + 6] * m.BottomMid +
                         pixelPtr[offset + sextupleStride + 9] * m.BottomMid +
                         pixelPtr[offset + sextupleStride + 15] * m.BottomRight +
                         pixelPtr[offset + sextupleStride + 18] * m.BottomRight)
                        / m.Factor + m.Offset;

            pixel = ClampPixel(pixel);

            return (byte)pixel;
        }

        /// <summary>
        /// Checks if the specified value is within the set bounds.
        /// </summary>
        /// <param name="value">The value being checked.</param>
        /// <returns>A value withing bounds.</returns>
        private static int ClampPixel(int value)
        {
            return value < 0 ? 0 : value > 255 ? 255 : value;
        }
    }
}
