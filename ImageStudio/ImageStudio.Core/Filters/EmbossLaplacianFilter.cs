﻿using System.Drawing;
using ImageStudio.Core.Enums;
using ImageStudio.Core.Models;

namespace ImageStudio.Core.Filters
{
    /// <summary>
    /// Implements the Emboss Laplacian filter.
    /// </summary>
    public static class EmbossLaplacianFilter
    {
        /// <summary>
        /// Peforms the Emboss Laplacian filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <param name="matrixSize">Determines what size convolution matrix to use.</param>
        /// <param name="inplace">Whether the filter is applied inplace or not.</param>
        /// <returns>The filtered image.</returns>
        public static Image EmbossLaplacian(Image image, ConvolutionMatrixSize matrixSize, bool inplace)
        {
            var m = new ConvolutionMatrix
            {
                Matrix = new[]
                {
                    new [] { -1 , 0 , -1 },
                    new [] { 0 , 4 , 0 },
                    new [] { -1 , 0 , -1 }
                },
                Factor = 1,
                Offset = 127
            };

            Image result;

            switch (matrixSize)
            {
                case ConvolutionMatrixSize.Conv3X3:
                    result = ConvolutionFilter.Convolution3X3(image, m, inplace);
                    break;
                case ConvolutionMatrixSize.Conv5X5:
                    m.Factor = 64;
                    m.Offset = 190;
                    result = ConvolutionFilter.Convolution5X5(image, m, inplace);
                    break;
                case ConvolutionMatrixSize.Conv7X7:
                    m.Factor = 127;
                    m.Offset = 200;
                    result = ConvolutionFilter.Convolution7X7(image, m, inplace);
                    break;
                default:
                    return default(Image);
            }

            return result;
        }
    }
}
