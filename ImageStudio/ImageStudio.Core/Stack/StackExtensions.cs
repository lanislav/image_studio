﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ImageStudio.Core.Commands;
using ImageStudio.Core.Enums;
using ImageStudio.Core.Utility;

namespace ImageStudio.Core.Stack
{
    /// <summary>
    /// Contains extension methods for working with the stack class.
    /// </summary>
    public static class StackExtensions
    {
        /// <summary>
        /// Gets the size of all the <see cref="Image"/> objects in the stack in bytes.
        /// </summary>
        /// <param name="stack">The stack whose size is being calculated.</param>
        /// <returns>The size of teh stack.</returns>
        public static double GetSizeInBytes(this Stack<ICommand> stack)
        {
            return stack.GetSize(ImageSize.B);
        }

        /// <summary>
        /// Gets the size of all the <see cref="Image"/> objects in the stack in kilo-bytes.
        /// </summary>
        /// <param name="stack">The stack whose size is being calculated.</param>
        /// <returns>The size of teh stack.</returns>
        public static double GetSizeInKiloBytes(this Stack<ICommand> stack)
        {
            return stack.GetSize(ImageSize.Kb);
        }

        /// <summary>
        /// Gets the size of all the <see cref="Image"/> objects in the stack in mega-bytes.
        /// </summary>
        /// <param name="stack">The stack whose size is being calculated.</param>
        /// <returns>The size of teh stack.</returns>
        public static double GetSizeInMegaBytes(this Stack<ICommand> stack)
        {
            return stack.GetSize(ImageSize.Mb);
        }

        /// <summary>
        /// Gets the size of all the <see cref="Image"/> objects in the stack in the specifeid <see cref="ImageSize"/> unit.
        /// </summary>
        /// <param name="stack">The stack whose size is being calculated.</param>
        /// <param name="imageSize">The <see cref="ImageSize"/> unit that is being calculated.</param>
        /// <returns>The size of teh stack.</returns>
        public static double GetSize(this Stack<ICommand> stack, ImageSize imageSize)
        {
            return stack.Select(x => x.UnExecute().GetSize(imageSize)).Sum();
        }
    }
}
