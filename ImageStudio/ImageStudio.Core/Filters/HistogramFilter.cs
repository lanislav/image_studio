﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using ImageStudio.Core.Models;

namespace ImageStudio.Core.Filters
{
    public static class HistogramFilter
    {
        public static Image Histogram(Image img, int rMin, int rMax, int gMin, int gMax, int bMin, int bMax)
        {
            CheckValues(rMin, rMax, gMin, gMax, bMin, bMax);

            var histogram = ChannelHistogram.GetChannelHistogram(img);

            var lowerBound = 127;
            var upperBound = 128;

            var rLowerVal = ArgMaxForRange(rMin, lowerBound, histogram.RedHistogram);
            var rUpperVal = ArgMaxForRange(upperBound, rMax, histogram.RedHistogram);
            var gLowerVal = ArgMaxForRange(gMin, lowerBound, histogram.GreenHistogram);
            var gUpperVal = ArgMaxForRange(upperBound, gMax, histogram.GreenHistogram);
            var bLowerVal = ArgMaxForRange(bMin, lowerBound, histogram.BlueHistogram);
            var bUpperVal = ArgMaxForRange(upperBound, bMax, histogram.BlueHistogram);

            using (var bmp = new Bitmap(img))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                if (p[2] < rMin)
                                {
                                    p[2] = (byte)rLowerVal;
                                }
                                else if (p[2] > rMax)
                                {
                                    p[2] = (byte)rUpperVal;
                                }

                                if (p[1] < gMin)
                                {
                                    p[1] = (byte)gLowerVal;
                                }
                                else if (p[1] > gMax)
                                {
                                    p[1] = (byte)gUpperVal;
                                }

                                if (p[0] < bMin)
                                {
                                    p[0] = (byte)bLowerVal;
                                }
                                else if (p[0] > bMax)
                                {
                                    p[0] = (byte)bUpperVal;
                                }

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        private static int ArgMaxForRange(int startInclusive, int endInclusive, int[] histogram)
        {
            var dict = new Dictionary<int, int>();
            var range = Enumerable.Range(startInclusive, endInclusive - startInclusive + 1).ToArray();

            for (var i = 0; i < histogram.Length; i++)
            {
                if (range.Contains(i))
                {
                    dict.Add(i, histogram[i]);
                }
            }

            var max = dict.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;

            return max;
        }

        private static void CheckValues(int rMin, int rMax, int gMin, int gMax, int bMin, int bMax)
        {
            if (rMin < 0 || rMin > 127)
            {
                throw new ArgumentOutOfRangeException(nameof(rMin), "Value is out of range.");
            }
            if (rMax < 128 || rMax > 255 )
            {
                throw new ArgumentOutOfRangeException(nameof(rMax), "Value is out of range.");
            }
            if (gMin < 0 || gMin > 127)
            {
                throw new ArgumentOutOfRangeException(nameof(gMin), "Value is out of range.");
            }
            if (gMax < 128 || gMax > 255)
            {
                throw new ArgumentOutOfRangeException(nameof(gMax), "Value is out of range.");
            }
            if (bMin < 0 || bMin > 127)
            {
                throw new ArgumentOutOfRangeException(nameof(bMin), "Value is out of range.");
            }
            if (bMax < 128 || bMax > 255)
            {
                throw new ArgumentOutOfRangeException(nameof(bMax), "Value is out of range.");
            }

            if (rMin > rMax || gMin > gMax || bMin > bMax)
            {
                throw new ArgumentException("Min. can not be larger than max. value.");
            }
        }
    }
}
