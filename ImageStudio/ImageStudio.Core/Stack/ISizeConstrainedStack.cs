﻿using System;
using System.Collections.Generic;

namespace ImageStudio.Core.Stack
{
    /// <summary>
    /// A generic interface for a stack of the specified type.
    /// </summary>
    /// <typeparam name="T">The type of the stack.</typeparam>
    public interface ISizeConstrainedStack<T>
    {
        Stack<T> Items { get; set; }
        double SizeConstraintInMb { get; set; }

        event EventHandler<StackReshapeEventArgs> OnReshape;
        bool Push(T image);
        T Pop();
        T Peek();
        void Clear();
        bool Any();
        void ResizeToFitSizeConstraint();
    }
}
