﻿namespace ImageStudio.GUI.Forms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBoxSizeConstraint = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblNote = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblConvNote = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioBtnNewPic = new System.Windows.Forms.RadioButton();
            this.radioBtnInplace = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.Location = new System.Drawing.Point(3, 1);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(380, 310);
            this.tabControl.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtBoxSizeConstraint);
            this.tabPage1.Controls.Add(this.lblDescription);
            this.tabPage1.Controls.Add(this.lblNote);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(372, 282);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "UndoRedo";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(332, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "MB";
            // 
            // txtBoxSizeConstraint
            // 
            this.txtBoxSizeConstraint.Location = new System.Drawing.Point(227, 25);
            this.txtBoxSizeConstraint.Name = "txtBoxSizeConstraint";
            this.txtBoxSizeConstraint.Size = new System.Drawing.Size(97, 21);
            this.txtBoxSizeConstraint.TabIndex = 9;
            this.txtBoxSizeConstraint.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtBoxSizeConstraint_KeyPress);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(7, 27);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(209, 16);
            this.lblDescription.TabIndex = 8;
            this.lblDescription.Text = "Maximum size of undo-redo stack:";
            // 
            // lblNote
            // 
            this.lblNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNote.Location = new System.Drawing.Point(10, 76);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(350, 56);
            this.lblNote.TabIndex = 7;
            this.lblNote.Text = "* Note: When the stack exceeds its maximum size, the oldest element in the stack " +
    "will be removed in order to make room for the new one.";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lblConvNote);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(372, 282);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Convolution";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblConvNote
            // 
            this.lblConvNote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblConvNote.Location = new System.Drawing.Point(10, 177);
            this.lblConvNote.Name = "lblConvNote";
            this.lblConvNote.Size = new System.Drawing.Size(350, 86);
            this.lblConvNote.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioBtnNewPic);
            this.groupBox1.Controls.Add(this.radioBtnInplace);
            this.groupBox1.Location = new System.Drawing.Point(11, 55);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(349, 109);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose mode:";
            // 
            // radioBtnNewPic
            // 
            this.radioBtnNewPic.AutoSize = true;
            this.radioBtnNewPic.Checked = true;
            this.radioBtnNewPic.Location = new System.Drawing.Point(22, 37);
            this.radioBtnNewPic.Name = "radioBtnNewPic";
            this.radioBtnNewPic.Size = new System.Drawing.Size(88, 19);
            this.radioBtnNewPic.TabIndex = 1;
            this.radioBtnNewPic.TabStop = true;
            this.radioBtnNewPic.Text = "New image";
            this.radioBtnNewPic.UseVisualStyleBackColor = true;
            this.radioBtnNewPic.CheckedChanged += new System.EventHandler(this.RadioBtnNewPic_CheckedChanged);
            // 
            // radioBtnInplace
            // 
            this.radioBtnInplace.AutoSize = true;
            this.radioBtnInplace.Location = new System.Drawing.Point(22, 62);
            this.radioBtnInplace.Name = "radioBtnInplace";
            this.radioBtnInplace.Size = new System.Drawing.Size(65, 19);
            this.radioBtnInplace.TabIndex = 0;
            this.radioBtnInplace.Text = "Inplace";
            this.radioBtnInplace.UseVisualStyleBackColor = true;
            this.radioBtnInplace.CheckedChanged += new System.EventHandler(this.RadioBtnInplace_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "Convolution filter mode:";
            // 
            // btnSave
            // 
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(202, 315);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(85, 25);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(293, 315);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(85, 25);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(384, 346);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Image Studio - Settings";
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBoxSizeConstraint;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioBtnInplace;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioBtnNewPic;
        private System.Windows.Forms.Label lblConvNote;
    }
}