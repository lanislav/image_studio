﻿using System;
using System.Globalization;
using System.Windows.Forms;
using ImageStudio.Core.Enums;

namespace ImageStudio.GUI.Forms
{
    public partial class SettingsForm : Form
    {
        private double _sizeConstraint;

        public double SizeConstraint
        {
            get => double.Parse(txtBoxSizeConstraint.Text);
            set
            {
                _sizeConstraint = value;

                txtBoxSizeConstraint.Text = _sizeConstraint.ToString(CultureInfo.InvariantCulture);
            }
        }

        public ConvolutionFilterMode FilterMode { get; set; }

        public SettingsForm()
        {
            InitializeComponent();

            // Default values.
            SizeConstraint = 100.0;
            FilterMode = ConvolutionFilterMode.NewImage;

            lblConvNote.Text =
                @"* New image: A completely new image is generated, without the newly calculated convolutional value affecting the rest of the calculations."
              + Environment.NewLine + 
                @"* Inplace: The newly calculated convolutional values will affect the next values that will be calculated.";
        }

        public SettingsForm(double sizeConstraint, ConvolutionFilterMode filterMode) : this()
        {
            SizeConstraint = sizeConstraint;
            FilterMode = filterMode;

            radioBtnNewPic.Checked = filterMode == ConvolutionFilterMode.NewImage;
            radioBtnInplace.Checked = filterMode == ConvolutionFilterMode.Inplace;
        }

        #region Event Handlers
        
        private void TxtBoxSizeConstraint_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            var textBox = (TextBox)sender;

            if (e.KeyChar == '.' && textBox.Text.IndexOf('.') > -1 && textBox.Text.IndexOf('.') == textBox.Text.LastIndexOf('.'))
            {
                e.Handled = true;
            }
        }

        private void RadioBtnNewPic_CheckedChanged(object sender, EventArgs e)
        {
            FilterMode = ConvolutionFilterMode.NewImage;
        }

        private void RadioBtnInplace_CheckedChanged(object sender, EventArgs e)
        {
            FilterMode = ConvolutionFilterMode.Inplace;
        }

        #endregion
    }
}
