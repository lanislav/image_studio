﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ImageStudio.GUI.Helpers
{
    /// <summary>
    /// Implements the functions for enabling window dragging.
    /// </summary>
    public static class WindowDragHelper
    {
        private const int WmNclbuttondown = 0xA1;
        private const int HtCaption = 0x2;

        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        private static extern bool ReleaseCapture();

        /// <summary>
        /// Starts to drag the object, until the mouse is released.
        /// </summary>
        /// <param name="sender">The object that sent the event.</param>
        /// <param name="e">The mouse event arguments.</param>
        /// <param name="handle">The <see cref="IntPtr"/> to a handle of the object.</param>
        public static void OnDraggableMouseDown(object sender, MouseEventArgs e, IntPtr handle)
        {
            if (e.Button != MouseButtons.Left)
            {
                return;
            }

            ReleaseCapture();
            SendMessage(handle, WmNclbuttondown, HtCaption, 0);
        }
    }
}
