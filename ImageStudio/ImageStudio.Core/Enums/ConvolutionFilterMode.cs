﻿namespace ImageStudio.Core.Enums
{
    public enum ConvolutionFilterMode
    {
        NewImage = 1,
        Inplace = 2
    }
}
