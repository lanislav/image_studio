﻿using System;

namespace ImageStudio.Core.AttributeModels
{
    /// <summary>
    /// Stores a set of four integers, that represent a <see cref="Margin"/> of a container.
    /// </summary>
    public class Margin
    {
        private int _left;
        private int _right;
        private int _top;
        private int _bottom;

        /// <summary>
        /// Initializes a new instance of the <see cref="Margin"/> class with all margins set to 0.
        /// </summary>
        public Margin() : this(0, 0, 0, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Margin"/> class with the specified horizontal and vertical margins, respectfully.
        /// </summary>
        /// <param name="horizontalMargin">Specifies the value of the left and right margins.</param>
        /// <param name="verticalMargin">Specified the value of the top and bottom margins.</param>
        public Margin(int horizontalMargin, int verticalMargin) : this(horizontalMargin, horizontalMargin,
            verticalMargin, verticalMargin)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Margin"/> class with the specified left, right, top and bottom margin values.
        /// </summary>
        /// <param name="left">Specifies the value of the left margin.</param>
        /// <param name="right">Specifies the value of the right margin.</param>
        /// <param name="top">Specifies the value of the top margin.</param>
        /// <param name="bottom">Specifies the value of the bottom margin.</param>
        public Margin(int left, int right, int top, int bottom)
        {
            CheckMargin(left);
            CheckMargin(right);
            CheckMargin(top);
            CheckMargin(bottom);

            _left = left;
            _right = right;
            _top = top;
            _bottom = bottom;
        }

        /// <summary>
        /// Checks if the specified value is a valid margin.
        /// </summary>
        /// <param name="margin">The margin value that is being checked.</param>
        private void CheckMargin(int margin)
        {
            if (margin < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(margin), "Value can not be less than 0.");
            }
        }
        
        /// <summary>
        /// Gets or sets the left margin property.
        /// </summary>
        public int Left
        {
            get => _left;
            set
            {
                CheckMargin(value);

                _left = value;
            }
        }

        /// <summary>
        /// Gets or sets the left right property.
        /// </summary>
        public int Right
        {
            get => _right;
            set
            {
                CheckMargin(value);

                _right = value;
            }
        }

        /// <summary>
        /// Gets or sets the left top property.
        /// </summary>
        public int Top
        {
            get => _top;
            set
            {
                CheckMargin(value);

                _top = value;
            }
        }

        /// <summary>
        /// Gets or sets the left bottom property.
        /// </summary>
        public int Bottom
        {
            get => _bottom;
            set
            {
                CheckMargin(value);

                _bottom = value;
            }
        }
    }
}
