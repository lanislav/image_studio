﻿using System.Drawing;
using System.Drawing.Imaging;

namespace ImageStudio.Core.Filters
{
    /// <summary>
    /// 
    /// </summary>
    public static class KuwaharaFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Image KuwaharaBlur(Image image, int size)
        {
            int[] apetureMinX = { -(size / 2), 0, -(size / 2), 0 };
            int[] apetureMaxX = { 0, size / 2, 0, size / 2 };
            int[] apetureMinY = { -(size / 2), -(size / 2), 0, 0 };
            int[] apetureMaxY = { 0, 0, size / 2, size / 2 };

            using (var bmp = new Bitmap(image))
            using (var newBmp = new Bitmap(bmp))
            {
                for (var x = 0; x < newBmp.Width; ++x)
                {
                    for (var y = 0; y < newBmp.Height; ++y)
                    {
                        int[] rValues = { 0, 0, 0, 0 };
                        int[] gValues = { 0, 0, 0, 0 };
                        int[] bValues = { 0, 0, 0, 0 };
                        int[] numPixels = { 0, 0, 0, 0 };
                        int[] maxRValue = { 0, 0, 0, 0 };
                        int[] maxGValue = { 0, 0, 0, 0 };
                        int[] maxBValue = { 0, 0, 0, 0 };
                        int[] minRValue = { 255, 255, 255, 255 };
                        int[] minGValue = { 255, 255, 255, 255 };
                        int[] minBValue = { 255, 255, 255, 255 };
                        for (var i = 0; i < 4; ++i)
                        {
                            for (var x2 = apetureMinX[i]; x2 < apetureMaxX[i]; ++x2)
                            {
                                var tmpX = x + x2;
                                if (tmpX >= 0 && tmpX < newBmp.Width)
                                {
                                    for (var y2 = apetureMinY[i]; y2 < apetureMaxY[i]; ++y2)
                                    {
                                        var tmpY = y + y2;
                                        if (tmpY >= 0 && tmpY < newBmp.Height)
                                        {
                                            var tmpColor = bmp.GetPixel(tmpX, tmpY);
                                            rValues[i] += tmpColor.R;
                                            gValues[i] += tmpColor.G;
                                            bValues[i] += tmpColor.B;
                                            if (tmpColor.R > maxRValue[i])
                                            {
                                                maxRValue[i] = tmpColor.R;
                                            }
                                            else if (tmpColor.R < minRValue[i])
                                            {
                                                minRValue[i] = tmpColor.R;
                                            }

                                            if (tmpColor.G > maxGValue[i])
                                            {
                                                maxGValue[i] = tmpColor.G;
                                            }
                                            else if (tmpColor.G < minGValue[i])
                                            {
                                                minGValue[i] = tmpColor.G;
                                            }

                                            if (tmpColor.B > maxBValue[i])
                                            {
                                                maxBValue[i] = tmpColor.B;
                                            }
                                            else if (tmpColor.B < minBValue[i])
                                            {
                                                minBValue[i] = tmpColor.B;
                                            }
                                            ++numPixels[i];
                                        }
                                    }
                                }
                            }
                        }
                        var j = 0;
                        var minDifference = 10000;
                        for (var i = 0; i < 4; ++i)
                        {
                            var currentDifference = maxRValue[i] - minRValue[i] + (maxGValue[i] - minGValue[i]) + (maxBValue[i] - minBValue[i]);
                            if (currentDifference < minDifference && numPixels[i] > 0)
                            {
                                j = i;
                                minDifference = currentDifference;
                            }
                        }

                        var meanPixel = Color.FromArgb(rValues[j] / numPixels[j],
                            gValues[j] / numPixels[j],
                            bValues[j] / numPixels[j]);
                        newBmp.SetPixel(x, y, meanPixel);
                    }
                }

                return new Bitmap(newBmp);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Image KuwaharaBlurUnsafe(Image image, int size)
        {
            int[] apetureMinX = { -(size / 2), 0, -(size / 2), 0 };
            int[] apetureMaxX = { 0, size / 2, 0, size / 2 };
            int[] apetureMinY = { -(size / 2), -(size / 2), 0, 0 };
            int[] apetureMaxY = { 0, 0, size / 2, size / 2 };

            using (var bmp = new Bitmap(image))
            using (var bmpCloned = new Bitmap(bmp))
            {
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                    ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                var bmpDataCloned = bmpCloned.LockBits(new Rectangle(0, 0, bmpCloned.Width, bmpCloned.Height),
                    ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmpData.Stride;

                var bmpScan0 = bmpData.Scan0;
                var bmpClonedScan0 = bmpDataCloned.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)bmpScan0;
                        var pCloned = (byte*)(void*)bmpClonedScan0;

                        var nOffset = stride - bmp.Width * 3;
                        var nWidth = bmp.Width * 3;

                        p += stride;
                        pCloned += stride;

                        for (var x = 0; x < bmp.Width; ++x)
                        {
                            for (var y = 0; y < bmp.Height; ++y)
                            {
                                int[] rValues = { 0, 0, 0, 0 };
                                int[] gValues = { 0, 0, 0, 0 };
                                int[] bValues = { 0, 0, 0, 0 };
                                int[] numPixels = { 0, 0, 0, 0 };
                                int[] maxRValue = { 0, 0, 0, 0 };
                                int[] maxGValue = { 0, 0, 0, 0 };
                                int[] maxBValue = { 0, 0, 0, 0 };
                                int[] minRValue = { 255, 255, 255, 255 };
                                int[] minGValue = { 255, 255, 255, 255 };
                                int[] minBValue = { 255, 255, 255, 255 };
                                for (var i = 0; i < 4; ++i)
                                {
                                    for (var x2 = apetureMinX[i]; x2 < apetureMaxX[i]; ++x2)
                                    {
                                        var tmpX = x + x2;
                                        if (tmpX < 0 || tmpX >= bmp.Width) continue;

                                        for (var y2 = apetureMinY[i]; y2 < apetureMaxY[i]; ++y2)
                                        {
                                            var tmpY = y + y2;
                                            if (tmpY < 0 || tmpY >= bmp.Height) continue;
                                            
                                            rValues[i] += pCloned[2];
                                            gValues[i] += pCloned[1];
                                            bValues[i] += pCloned[0];
                                            if (pCloned[2] > maxRValue[i])
                                            {
                                                maxRValue[i] = pCloned[2];
                                            }
                                            else if (pCloned[2] < minRValue[i])
                                            {
                                                minRValue[i] = pCloned[2];
                                            }

                                            if (pCloned[1] > maxGValue[i])
                                            {
                                                maxGValue[i] = pCloned[1];
                                            }
                                            else if (pCloned[1] < minGValue[i])
                                            {
                                                minGValue[i] = pCloned[1];
                                            }

                                            if (pCloned[0] > maxBValue[i])
                                            {
                                                maxBValue[i] = pCloned[0];
                                            }
                                            else if (pCloned[0] < minBValue[i])
                                            {
                                                minBValue[i] = pCloned[0];
                                            }
                                            ++numPixels[i];
                                        }
                                    }
                                }
                                var j = 0;
                                var minDifference = 10000;
                                for (var i = 0; i < 4; ++i)
                                {
                                    var currentDifference = maxRValue[i] - minRValue[i] + (maxGValue[i] - minGValue[i]) + (maxBValue[i] - minBValue[i]);
                                    if (currentDifference >= minDifference || numPixels[i] <= 0) continue;

                                    j = i;
                                    minDifference = currentDifference;
                                }

                                p[2] = (byte) (rValues[j] / numPixels[j]);
                                p[1] = (byte) (gValues[j] / numPixels[j]);
                                p[0] = (byte) (bValues[j] / numPixels[j]);

                                ++p;
                                ++pCloned;
                            }

                            p += 3 + nOffset;
                            pCloned += 3 + nOffset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmpData);
                        bmpCloned.UnlockBits(bmpDataCloned);
                    }
                }

                return new Bitmap(bmp);
            }
        }
    }
}
