﻿using System;

namespace ImageStudio.Core.Stack
{
    public class StackReshapeEventArgs : EventArgs
    {
        public int RemovedItemsCount { get; set; }

        public StackReshapeEventArgs(int removedItemsCount)
        {
            RemovedItemsCount = removedItemsCount;
        }
    }
}
