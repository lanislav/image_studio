﻿using System;
using System.Drawing;

namespace ImageStudio.Core.AttributeModels
{
    public class ImageContainer
    {
        private int _width;
        private int _height;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageContainer"/> class with the specified width and height.
        /// </summary>
        /// <param name="width">The width of the container.</param>
        /// <param name="height">The height of the container.</param>
        public ImageContainer(int width, int height)
        {
            CheckValue(width);
            CheckValue(Height);

            _width = width;
            _height = height;

            ImageDimensions = new Size(0, 0);
            Margin = new Margin();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageContainer"/> class with the specified height and width,
        /// and a <see cref="Size"/> structure representing the size of the containers image.
        /// </summary>
        /// <param name="imageDimensions">The dimensions of the image that will fit inside the container.</param>
        /// <param name="width">The width of the container.</param>
        /// <param name="height">The height of the container.</param>
        public ImageContainer(Size imageDimensions, int width, int height)
        {
            CheckValue(width);
            CheckValue(Height);

            _width = width;
            _height = height;
            ImageDimensions = imageDimensions;
            Margin = new Margin();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageContainer"/> class with the specified height and width,
        /// and a <see cref="Size"/> structure specifying the size of the containers image,
        /// and a <see cref="Margin"/> object specifiying the margin of the container.
        /// </summary>
        /// <param name="imageDimensions">The dimensions of the image that will fit inside the container.</param>
        /// <param name="width">The width of the container.</param>
        /// <param name="height">The height of the container.</param>
        /// <param name="margin">The <see cref="Margin"/> of the container.</param>
        public ImageContainer(Size imageDimensions, int width, int height, Margin margin) : this (imageDimensions, width, height)
        {
            Margin = margin;
        }

        /// <summary>
        /// Checks if the specified value is valid.
        /// </summary>
        /// <param name="value">The value that is being checked.</param>
        private void CheckValue(int value)
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "Value can not be less than 0.");
            }
        }

        /// <summary>
        /// Gets or sets the width property.
        /// </summary>
        public int Width
        {
            get => _width;
            set
            {
                CheckValue(value);

                _width = value;
            }
        }

        /// <summary>
        /// Gets or sets the height property.
        /// </summary>
        public int Height
        {
            get => _height;
            set
            {
                CheckValue(value);

                _height = value;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="Margin"/> property.
        /// </summary>
        public Margin Margin { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="Size"/> property.
        /// </summary>
        public Size ImageDimensions { get; set; }

        /// <summary>
        /// Calculates the size of a <see cref="Rectangle"/> so that the image will fit in the center of the container.
        /// </summary>
        /// <returns>A new <see cref="Rectangle"/> object.</returns>
        public Rectangle FitImageToCenterOfContainer()
        {
            if (ImageDimensions.Width == 0 && ImageDimensions.Height == 0)
            {
                return new Rectangle(0, 0, Width, Height);
            }
            
            var xOffsetToCenter = Math.Abs(Width - ImageDimensions.Width) / 2;
            var yOffsetToCenter = Math.Abs(Height - ImageDimensions.Height) / 2;

            return new Rectangle(Margin.Left + xOffsetToCenter, Margin.Top + yOffsetToCenter, ImageDimensions.Width,
                ImageDimensions.Height);
        }
    }
}
