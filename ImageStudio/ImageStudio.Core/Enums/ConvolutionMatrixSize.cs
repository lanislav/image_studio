﻿namespace ImageStudio.Core.Enums
{
    public enum ConvolutionMatrixSize
    {
        Conv3X3 = 1,
        Conv5X5 = 2,
        Conv7X7 = 3
    }
}
