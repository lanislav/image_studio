﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ImageStudio.Core.Enums;

namespace ImageStudio.Core.Utility
{
    /// <summary>
    /// Contains basic extension methods for the <see cref="Image"/> class.
    /// </summary>
    public static class ImageExtensions
    {
        /// <summary>
        /// Resizes the <see cref="Image"/> to a fixed size.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being resized.</param>
        /// <param name="width">The width of the new image.</param>
        /// <param name="height">The height of the new image.</param>
        /// <returns>A new <see cref="Image"/> object of the specified size.</returns>
        public static Image Resize(this Image image, int width, int height)
        {
            return ImageTransformation.Resize(image.ToStream(), width, height);
        }

        /// <summary>
        /// Converts the <see cref="Image"/> instance to a <see cref="Stream"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being converted to a stream.</param>
        /// <returns>A new <see cref="Stream"/>.</returns>
        public static Stream ToStream(this Image image)
        {
           return new MemoryStream(image.ToByteArray());
        }

        /// <summary>
        /// Converts the <see cref="Image"/> to an array of bytes.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being converted to a stream.</param>
        /// <returns>An array of bytes.</returns>
        public static byte[] ToByteArray(this Image image)
        {
            return image.ToByteArray(ImageFormat.Png);
        }

        /// <summary>
        /// Converts the <see cref="Image"/> to an array of bytes.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being converted to a stream.</param>
        /// <param name="format">The <see cref="ImageFormat"/> of the resulting byte array.</param>
        /// <returns>An array of bytes.</returns>
        public static byte[] ToByteArray(this Image image, ImageFormat format)
        {
            using (var memoryStream = new MemoryStream())
            {
                image.Save(memoryStream, format);

                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Gets the red channel of the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose red channel is being extracted.</param>
        /// <returns>A red channel <see cref="Image"/>.</returns>
        public static Image GetRedChannel(this Image image)
        {
            return image.GetChannel(ImageColorChannel.R);
        }

        /// <summary>
        /// Gets the green channel of the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose green channel is being extracted.</param>
        /// <returns>A green channel <see cref="Image"/>.</returns>
        public static Image GetGreenChannel(this Image image)
        {
            return image.GetChannel(ImageColorChannel.G);
        }

        /// <summary>
        /// Gets the blue channel of the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose blue channel is being extracted.</param>
        /// <returns>A blue channel <see cref="Image"/>.</returns>
        public static Image GetBlueChannel(this Image image)
        {
            return image.GetChannel(ImageColorChannel.B);
        }

        /// <summary>
        /// Gets the <see cref="ImageColorChannel"/> channel of the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose <see cref="ImageColorChannel"/> channel is being extracted.</param>
        /// <param name="colorChannel">The <see cref="ImageColorChannel"/> that is being extracted.</param>
        /// <returns>A <see cref="ImageColorChannel"/> channel <see cref="Image"/>.</returns>
        public static Image GetChannel(this Image image, ImageColorChannel colorChannel)
        {
            return ImageUtility.GetChannel(image, colorChannel);
        }

        /// <summary>
        /// Gets the red channel of the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose red channel is being extracted.</param>
        /// <returns>A red channel <see cref="Image"/>.</returns>
        public static Image GetRedChannelUnsafe(this Image image)
        {
            return image.GetChannelUnsafe(ImageColorChannel.R);
        }

        /// <summary>
        /// Gets the green channel of the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose green channel is being extracted.</param>
        /// <returns>A green channel <see cref="Image"/>.</returns>
        public static Image GetGreenChannelUnsafe(this Image image)
        {
            return image.GetChannelUnsafe(ImageColorChannel.G);
        }

        /// <summary>
        /// Gets the blue channel of the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose blue channel is being extracted.</param>
        /// <returns>A blue channel <see cref="Image"/>.</returns>
        public static Image GetBlueChannelUnsafe(this Image image)
        {
            return image.GetChannelUnsafe(ImageColorChannel.B);
        }

        /// <summary>
        /// Gets the <see cref="ImageColorChannel"/> channel of the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose <see cref="ImageColorChannel"/> channel is being extracted.</param>
        /// <param name="colorChannel">The <see cref="ImageColorChannel"/> that is being extracted.</param>
        /// <returns>A <see cref="ImageColorChannel"/> channel <see cref="Image"/>.</returns>
        public static Image GetChannelUnsafe(this Image image, ImageColorChannel colorChannel)
        {
            return ImageUtility.GetChannelUnsafe(image, colorChannel);
        }

        /// <summary>
        /// Gets the size of an individual pixel in bytes, of the specified <see cref="Image"/> object.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> object whose pixel size we are calculating.</param>
        /// <returns>The size of the pixel in bytes.</returns>
        public static int GetPixelSize(this Image image)
        {
            return ImageUtility.GetPixelSize(image);
        }

        /// <summary>
        /// Gets the size in bytes of the <see cref="Image"/> object.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose size is being calculated.</param>
        /// <returns>The size of the image in bytes.</returns>
        public static double GetSizeInBytes(this Image image)
        {
            return image.GetSize(ImageSize.B);
        }

        /// <summary>
        /// Gets the size in kilo-bytes of the <see cref="Image"/> object.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose size is being calculated.</param>
        /// <returns>The size of the image in kilo-bytes.</returns>
        public static double GetSizeInKiloBytes(this Image image)
        {
            return image.GetSize(ImageSize.Kb);
        }

        /// <summary>
        /// Gets the size in mega-bytes of the <see cref="Image"/> object.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose size is being calculated.</param>
        /// <returns>The size of the image in mega-bytes.</returns>
        public static double GetSizeInMegaBytes(this Image image)
        {
            return image.GetSize(ImageSize.Mb);
        }

        /// <summary>
        /// Gets the size in <see cref="ImageSize"/> units of the <see cref="Image"/> object.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose size is being calculated.</param>
        /// <param name="imageSize">The <see cref="ImageSize"/> type to calculate.</param>
        /// <returns>The size of the image in <see cref="ImageSize"/>.</returns>
        public static double GetSize(this Image image, ImageSize imageSize)
        {
            return ImageUtility.GetSize(image, imageSize);
        }

        /// <summary>
        /// Pads the image with the specified padding.
        /// </summary>
        /// <param name="image">The image being padded.</param>
        /// <param name="padding">The padding added to the image.</param>
        /// <returns>A new padded image.</returns>
        public static Image Pad(this Image image, int padding)
        {
            return ImageUtility.PadImage(image, padding);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static Image Monochrome(this Image image)
        {
            return ImageUtility.Monochrome(image);
        }
    }
}
