﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using ImageStudio.Core.Utility;

namespace ImageStudio.Core.Filters
{
    public static class StuckiDitheringFilter
    {
        private static readonly int[][] StuckiDitheringMatrix = {
            new [] { 0, 0, 0, 8, 4 },
            new [] { 2, 4, 8, 4, 2 },
            new [] { 1, 2, 4, 2, 1 },
        };

        private const int StuckiDitheringFactor = 48;
        
        public static Image Dither(Image image)
        {
            using (var bmp = new Bitmap(image))
            using (var bmpMonochrome = new Bitmap(image.Monochrome()))
            {
                int redError, greenError, blueError, xCoord, yCoord, coefficient;
                for (var y = 0; y < bmp.Height; y++)
                {
                    for (var x = 0; x < bmp.Width; x++)
                    {
                        var clr = bmp.GetPixel(x, y);
                        var clrMono = bmpMonochrome.GetPixel(x, y);

                        redError = clr.R - clrMono.R;
                        greenError = clr.G - clrMono.G;
                        blueError = clr.B - clrMono.B;

                        for (var p = 0; p < StuckiDitheringMatrix.Length; p++)
                        {
                            for (var k = 0; k < StuckiDitheringMatrix[0].Length; k++)
                            {
                                xCoord = x - 2;
                                yCoord = y + p;

                                if (StuckiDitheringMatrix[p][k] == 0 || yCoord > bmp.Height - 2 || xCoord < 0 || xCoord > bmp.Width - 2)
                                {
                                    continue;
                                }

                                coefficient = StuckiDitheringMatrix[p][k];
                                redError = redError * coefficient / StuckiDitheringFactor;
                                greenError = greenError * coefficient / StuckiDitheringFactor;
                                blueError = blueError * coefficient / StuckiDitheringFactor;

                                var oldClr = bmp.GetPixel(xCoord, yCoord);

                                var newRed = oldClr.R + (byte) redError;
                                var newGreen = oldClr.G + (byte)greenError;
                                var newBlue = oldClr.B + (byte)blueError;

                                bmp.SetPixel(xCoord, yCoord, Color.FromArgb((byte)newRed, (byte)newGreen, (byte)newBlue));
                            }
                        }
                    }
                }

                return new Bitmap(bmp);
            }
        }

        public static Image DitherUnsafe(Image image)
        {
            using (var bmp = new Bitmap(image))
            using (var bmpMonochrome = new Bitmap(image.Monochrome()))
            {
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                var bmpMonochromeData = bmpMonochrome.LockBits(new Rectangle(0, 0, bmpMonochrome.Width, bmpMonochrome.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var bmpStride = bmpData.Stride;
                var bmpMonochromeStride = bmpMonochromeData.Stride;

                var bmpScan0 = bmpData.Scan0;
                var bmpMonochromeScan0 = bmpMonochromeData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)bmpScan0;
                        var pMonochrome = (byte*)(void*)bmpMonochromeScan0;
                        var pixelSize = 3;

                        var offset = bmpStride - bmp.Width * pixelSize;
                        var offsetMonochrome = bmpMonochromeStride - bmpMonochrome.Width * pixelSize;

                        var height = bmp.Height;
                        var width = bmp.Width;

                        int redError, greenError, blueError, xCoord, pixelOffset, coefficient;
                        for (var y = 0; y < height; ++y)
                        {
                            for (var x = 0; x < width; ++x)
                            {
                                redError = p[2] - pMonochrome[2];
                                greenError = p[1] - pMonochrome[1];
                                blueError = p[0] - pMonochrome[0];

                                for (var i = 0; i < StuckiDitheringMatrix.Length; i++)
                                {
                                    for (var j = 0; j < StuckiDitheringMatrix[0].Length; j++)
                                    {
                                        xCoord = j - 2;

                                        if (StuckiDitheringMatrix[i][j] == 0 || xCoord + x >  width || xCoord + x < 0 || y > height - 3)
                                        {
                                            continue;
                                        }

                                        coefficient = StuckiDitheringMatrix[i][j];
                                        pixelOffset = i * bmpStride + xCoord * 3;
                                        redError = redError * coefficient / StuckiDitheringFactor;
                                        greenError = greenError * coefficient / StuckiDitheringFactor;
                                        blueError = blueError * coefficient / StuckiDitheringFactor;

                                        p[2 + pixelOffset] = (byte)(p[2 + pixelOffset] + (byte)redError);
                                        p[1 + pixelOffset] = (byte)(p[1 + pixelOffset] + (byte)greenError);
                                        p[0 + pixelOffset] = (byte)(p[0 + pixelOffset] + (byte)blueError);
                                    }
                                }

                                p += pixelSize;
                                pMonochrome += pixelSize;
                            }

                            p += offset;
                            pMonochrome += offsetMonochrome;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmpData);
                        bmpMonochrome.UnlockBits(bmpMonochromeData);
                    }
                }

                return new Bitmap(bmp);
            }
        }
    }
}
