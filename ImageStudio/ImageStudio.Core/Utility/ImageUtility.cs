﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using ImageStudio.Core.Enums;

namespace ImageStudio.Core.Utility
{
    /// <summary>
    /// Contains utility methods for the <see cref="Image"/> class.
    /// </summary>
    public static class ImageUtility
    {
        public static Image GetChannelUnsafe(Image image, ImageColorChannel channel)
        {
            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        var validIndexes = new[] {0, 1, 2};

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                foreach (var index in validIndexes)
                                {
                                    p[index] = index == (int) channel ? (byte)(255 - p[index]) : (byte)0;
                                }

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Gets the specified <see cref="ImageColorChannel"/> of the <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose channel is being extracted.</param>
        /// <param name="colorChannel">The <see cref="ImageColorChannel"/> that we are extracting.</param>
        /// <returns>A <see cref="Image"/> of the specified color channel.</returns>
        public static Image GetChannel(Image image, ImageColorChannel colorChannel)
        {
            using (var bmp = new Bitmap(image))
            using (var channelBmp = new Bitmap(image.Width, image.Height))
            {
                for (var x = 0; x < bmp.Width; x++)
                {
                    for (var y = 0; y < bmp.Height; y++)
                    {
                        var pxl = bmp.GetPixel(x, y);

                        switch (colorChannel)
                        {
                            case ImageColorChannel.R:
                                var redPxl = Color.FromArgb(255 - pxl.R, 0, 0);
                                channelBmp.SetPixel(x, y, redPxl);
                                break;
                            case ImageColorChannel.G:
                                var greenPxl = Color.FromArgb(0, 255 - pxl.G, 0);
                                channelBmp.SetPixel(x, y, greenPxl);
                                break;
                            case ImageColorChannel.B:
                                var bluePxl = Color.FromArgb(0, 0, 255 - pxl.B);
                                channelBmp.SetPixel(x, y, bluePxl);
                                break;
                        }
                    }
                }

                return new Bitmap(channelBmp);
            }
        }

        /// <summary>
        /// Gets the size of the specified <see cref="Image"/> in the specified <see cref="ImageSize"/> unit.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> whose channel is being extracted.</param>
        /// <param name="imageSize">The <see cref="ImageSize"/> unit.</param>
        /// <returns>The size of the image.</returns>
        public static double GetSize(Image image, ImageSize imageSize)
        {
            var depth = Image.GetPixelFormatSize(image.PixelFormat);

            var denominator = 8;

            switch (imageSize)
            {
                case ImageSize.Kb:
                    denominator *= 1024;
                    break;
                case ImageSize.Mb:
                    denominator *= 1024 * 1024;
                    break;
            }

            var size = image.Width * image.Height * depth / (double)denominator;

            return size;
        }

        /// <summary>
        /// Gets the size of an individual pixel in bytes, of the specified <see cref="Image"/> object.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> object whose pixel size we are calculating.</param>
        /// <returns>The size of the pixel in bytes.</returns>
        public static int GetPixelSize(Image image)
        {
            var depth = Image.GetPixelFormatSize(image.PixelFormat);

            var result = depth > 0 ?  depth / 8 : default(int);

            return result;
        }

        /// <summary>
        /// Pads the image with the specified padding.
        /// </summary>
        /// <param name="image">The image being padded.</param>
        /// <param name="padding">The padding added to the image.</param>
        /// <returns>A new padded image.</returns>
        public static Image PadImage(Image image, int padding)
        {
            if (padding < 0 || padding % 2 != 0)
            {
                throw new ArgumentException("Padding must be greater than 0, and an even number.", nameof(padding));
            }

            var newWidth = image.Width + padding;
            var newHeight = image.Height + padding;

            var bmp = new Bitmap(newWidth, newHeight);
            var coord = padding / 2;

            // Fill in the image.
            using (var g = Graphics.FromImage(bmp))
            {
                g.DrawImage(image, coord, coord);
            }

            // Handle the edges.
            using (var temp = new Bitmap(image))
            {
                for (var i = 0; i < padding / 2; i++)
                {
                    for (var y = 0; y < temp.Height; y++)
                    {
                        bmp.SetPixel(i, y, temp.GetPixel(padding - i, y));
                        bmp.SetPixel(newWidth - 1 - i, y, temp.GetPixel(temp.Width - 1 - i, y));
                    }

                    for (var x = 0; x < temp.Width; x++)
                    {
                        bmp.SetPixel(x, i, temp.GetPixel(x, i));
                        bmp.SetPixel(x, newHeight - 1 - i, temp.GetPixel(x , temp.Height - 1 - i));
                    }
                }
            }

            return bmp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static Image Monochrome(Image image)
        {
            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        byte gray;
                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                gray = (byte)(0.299 * p[2] + 0.587 * p[1] + 0.114 * p[0]);
                                gray = gray < 128 ? (byte)0 : (byte)255;

                                p[0] = gray;
                                p[1] = gray;
                                p[2] = gray;

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }

                return new Bitmap(bmp);
            }
        }
    }
}
