﻿using System.Drawing;
using ImageStudio.Core.Enums;
using ImageStudio.Core.Filters;

namespace ImageStudio.Core.Commands
{
    /// <summary>
    /// Implements the <see cref="ICommand"/> interface for the <see cref="EmbossLaplacianFilter"/>.
    /// </summary>
    public class EmbossLaplacianCommand : ICommand
    {
        private readonly Image _image;
        private readonly ConvolutionMatrixSize _size;
        private readonly bool _inplace;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmbossLaplacianCommand"/> class.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> the command will be executed on.</param>
        /// <param name="size">The size of the convolution matrix.</param>
        /// <param name="inplace">Whether the filter is applied inplace or not.</param>
        public EmbossLaplacianCommand(Image image, ConvolutionMatrixSize size, bool inplace)
        {
            _image = image;
            _size = size;
            _inplace = inplace;
        }

        /// <summary>
        /// Gets the name property.
        /// </summary>
        public string Name => "Emboss Laplacian";

        /// <summary>
        /// Executes the filter.
        /// </summary>
        /// <returns>The filtered image.</returns>
        public Image Execute()
        {
            return EmbossLaplacianFilter.EmbossLaplacian(_image, _size, _inplace);
        }

        /// <summary>
        /// Unexecutes the filter.
        /// </summary>
        /// <returns>The original image.</returns>
        public Image UnExecute()
        {
            return _image;
        }
    }
}
