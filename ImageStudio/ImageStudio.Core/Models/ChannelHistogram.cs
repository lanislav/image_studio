﻿using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using ImageStudio.Core.Enums;

namespace ImageStudio.Core.Models
{
    public class ChannelHistogram
    {
        public int[] RedHistogram { get; set; }
        public int[] BlueHistogram { get; set; }
        public int[] GreenHistogram { get; set; }

        public ChannelHistogram()
        {
            RedHistogram = new int[256];
            BlueHistogram = new int[256];
            GreenHistogram = new int[256];
        }

        public static ChannelHistogram GetChannelHistogramCmy(Image image)
        {
            var histogram = new ChannelHistogram();

            if (image == null)
            {
                return histogram;
            }

            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                histogram.BlueHistogram[255 - p[0]]++;
                                histogram.GreenHistogram[255 - p[1]]++;
                                histogram.RedHistogram[255 - p[2]]++;

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }
            }

            return histogram;
        }

        public static ChannelHistogram GetChannelHistogram(Image image)
        {
            var histogram = new ChannelHistogram();

            if (image == null)
            {
                return histogram;
            }

            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                histogram.BlueHistogram[p[0]]++;
                                histogram.GreenHistogram[p[1]]++;
                                histogram.RedHistogram[p[2]]++;

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }
            }

            return histogram;
        }

        public static void DrawHistogram(int[] values, Graphics g, Rectangle rect, ImageColorChannel channel)
        {
            var padding = 20;
            var xUnit = (rect.Width - padding) / 256.0f;
            var yUnit = (rect.Height - padding) / (float)values.Max();
            var color = Color.FromArgb(channel == ImageColorChannel.R ? 255 : 0, channel == ImageColorChannel.G ? 255 : 0, channel == ImageColorChannel.B ? 255 : 0);
            var pen = new Pen(new SolidBrush(color), xUnit);

            var xOffset = (float) (rect.Left + padding / 2);
            var bottom = (float) (rect.Bottom - padding / 2);
            foreach (var val in values)
            {
                g.DrawLine(pen, 
                    new PointF(xOffset, bottom), 
                    new PointF(xOffset, bottom - val * yUnit));

                xOffset += xUnit;
            }

            g.DrawRectangle(new Pen(new SolidBrush(Color.Black), 1), rect.Left, rect.Top,
                rect.Width - 1, rect.Height - 1);
        }
    }
}
