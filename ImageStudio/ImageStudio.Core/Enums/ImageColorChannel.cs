﻿namespace ImageStudio.Core.Enums
{
    public enum ImageColorChannel
    {
        B = 0,
        G = 1,
        R = 2
    }
}
