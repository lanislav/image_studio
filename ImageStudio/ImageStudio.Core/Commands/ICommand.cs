﻿using System.Drawing;

namespace ImageStudio.Core.Commands
{
    /// <summary>
    /// An interface for filter commands.
    /// </summary>
    public interface ICommand
    {
        string Name { get;  }
        Image Execute();
        Image UnExecute();
    }
}
