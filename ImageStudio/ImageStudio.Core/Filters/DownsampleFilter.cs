﻿using System.Drawing;
using System.Drawing.Imaging;
using ImageStudio.Core.Enums;

namespace ImageStudio.Core.Filters
{
    public static class DownsampleFilter
    {
        public static Image Downsample(Image image, ImageColorChannel channel)
        {
            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;
                        var n = 0;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                switch (channel)
                                {
                                    case ImageColorChannel.B:
                                        p[2] = (byte)(n % 2 == 0 ? p[2] : 0);
                                        p[1] = (byte)(n % 2 == 0 ? p[1] : 0);
                                        break;
                                    case ImageColorChannel.G:
                                        p[2] = (byte)(n % 2 == 0 ? p[2] : 0);
                                        p[0] = (byte)(n % 2 == 0 ? p[0] : 0);
                                        break;
                                    case ImageColorChannel.R:
                                        p[1] = (byte)(n % 2 == 0 ? p[1] : 0);
                                        p[0] = (byte)(n % 2 == 0 ? p[0] : 0);
                                        break;
                                }

                                p += 3;
                                n++;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }

                return new Bitmap(bmp);
            }
        }
    }
}
