﻿using System;
using System.Drawing;

namespace ImageStudio.Core.Filters
{
    public static class SimilarZoneEqualizationFilter
    {
        public static Image Equalize(Image image, Color color, int ptX, int ptY, double similarityThreshold)
        {
            if (!WithinBoundsOfImage(ptX, ptY, image.Width, image.Height))
            {
                return new Bitmap(image);
            }

            using (var bmp = new Bitmap(image))
            {
                var oldColor = bmp.GetPixel(ptX, ptY);
                bmp.SetPixel(ptX, ptY, color);

                for (var i = -1; i < 2; i++)
                {
                    for (var j = -1; j < 2; j++)
                    {
                        if (i == 0 && j == 0) continue;
                        
                        EqualizeRecursive(bmp, oldColor, color, ptX + i, ptY + j, similarityThreshold);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        private static void EqualizeRecursive(Bitmap bmp, Color oldColor, Color color, int ptX, int ptY, double similarityThreshold)
        {
            if (!WithinBoundsOfImage(ptX, ptY, bmp.Width, bmp.Height))
            {
                return;
            }

            var current = bmp.GetPixel(ptX, ptY);
            var similarity = GetSimilarity(current, oldColor);

            if (similarity > similarityThreshold)
            {
                return;
            }

            bmp.SetPixel(ptX, ptY, color);

            for (var i = -1; i < 2; i++)
            {
                for (var j = -1; j < 2; j++)
                {
                    if (i == 0 && j == 0) continue;

                    EqualizeRecursive(bmp, oldColor, color, ptX + i, ptY + j, similarityThreshold);
                }
            }
        }

        private static bool WithinBoundsOfImage(int x, int y, int width, int height)
        {
            return x >= 0 && y >= 0 && x <= width && y <= height;
        }

        private static double GetSimilarity(Color a, Color b)
        {
            return GetSimilarity(a.R, a.G, a.B, b.R, b.G, b.B);
        }

        private static double GetSimilarity(byte r1, byte g1, byte b1, byte r2, byte g2, byte b2)
        {
            var r = (r1 - r2) * (r1 - r2);
            var g = (g1 - g2) * (g1 - g2);
            var b = (b1 - b2) * (b1 - b2);

            var result = Math.Sqrt(r + g + b);

            return result;
        }
    }
}
