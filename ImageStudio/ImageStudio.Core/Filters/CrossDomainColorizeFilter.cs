﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace ImageStudio.Core.Filters
{
    public static class CrossDomainColorizeFilter
    {
        public static Image Colorize(Image image, double hue, double? saturation = null)
        {
            using (var bmp = new Bitmap(image))
            {
                Color color;
                double h, s, v;
                for (var x = 0; x < image.Width; x++)
                {
                    for (var y = 0; y < image.Height; y++)
                    {
                        color = bmp.GetPixel(x, y);
                        ColorToHsv(color, out h, out s, out v);

                        h = hue;
                        h = h < 0 ? 0 :
                            h > 360 ? 360 : h;
                        s = s < 0 ? 0 :
                            s > 100 ? 100 : s;
                        v = v < 0 ? 0 :
                            v > 255 ? 255 : v;

                        saturation = saturation > 100 ? 100 : saturation;

                        bmp.SetPixel(x, y, ColorFromHsv(h, saturation ?? s, v));
                    }
                }

                return new Bitmap(bmp);
            }
        }

        public static Image ColorizeUnsafe(Image image, double hue, double? saturation = null)
        {
            using (var bmp = new Bitmap(image))
            {
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmpData.Stride;
                var scan0 = bmpData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        Color color, newColor;
                        double h, s, v;
                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                color = Color.FromArgb(p[2], p[1], p[0]);
                                ColorToHsv(color, out h, out s, out v);

                                h = hue;
                                h = h < 0 ? 0 :
                                    h > 360 ? 360 : h;
                                s = s < 0 ? 0 :
                                    s > 100 ? 100 : s;
                                v = v < 0 ? 0 :
                                    v > 255 ? 255 : v;

                                saturation = saturation > 100 ? 100 : saturation;

                                newColor = ColorFromHsv(h, saturation ?? s, v);

                                p[0] = newColor.B;
                                p[1] = newColor.G;
                                p[2] = newColor.R;

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmpData);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        private static void ColorToHsv(Color color, out double hue, out double saturation, out double value)
        {
            int max = Math.Max(color.R, Math.Max(color.G, color.B));
            int min = Math.Min(color.R, Math.Min(color.G, color.B));

            hue = color.GetHue();
            saturation = max == 0 ? 0 : 1d - 1d * min / max;
            value = max / 255d;
        }

        private static Color ColorFromHsv(double hue, double saturation, double value)
        {
            var high = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            var f = hue / 60 - Math.Floor(hue / 60);

            value = value * 255;
            var v = Convert.ToInt32(value);
            var p = Convert.ToInt32(value * (1 - saturation));
            var q = Convert.ToInt32(value * (1 - f * saturation));
            var t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

            switch (high)
            {
                case 0:
                    return Color.FromArgb(255, v, t, p);
                case 1:
                    return Color.FromArgb(255, q, v, p);
                case 2:
                    return Color.FromArgb(255, p, v, t);
                case 3:
                    return Color.FromArgb(255, p, q, v);
                case 4:
                    return Color.FromArgb(255, t, p, v);
                default:
                    return Color.FromArgb(255, v, p, q);
            }
        }
    }
}
