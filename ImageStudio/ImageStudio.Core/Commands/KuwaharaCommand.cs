﻿using System.Drawing;
using ImageStudio.Core.Filters;

namespace ImageStudio.Core.Commands
{
    public class KuwaharaCommand : ICommand
    {
        private readonly Image _image;
        private readonly bool _safeMode;
        private readonly int _size;

        /// <summary>
        /// Initializes a new instance of the <see cref="KuwaharaCommand"/> class.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> the command will be executed on.</param>
        /// <param name="safeMode">Whether the command runs in safe mode or not.</param>
        /// <param name="size"></param>
        public KuwaharaCommand(Image image, bool safeMode, int size)
        {
            _image = image;
            _safeMode = safeMode;
            _size = size;
        }

        /// <summary>
        /// Gets the name property.
        /// </summary>
        public string Name => "Kuwahara";

        /// <summary>
        /// Executes the filter.
        /// </summary>
        /// <returns>The filtered image.</returns>
        public Image Execute()
        {
            return _safeMode
                ? KuwaharaFilter.KuwaharaBlur(_image, _size)
                : KuwaharaFilter.KuwaharaBlurUnsafe(_image, _size);
        }

        /// <summary>
        /// Unexecutes the filter.
        /// </summary>
        /// <returns>The original image.</returns>
        public Image UnExecute()
        {
            return _image;
        }
    }
}
