﻿namespace ImageStudio.Core.Enums
{
    public enum ImageSize
    {
        B = 0,
        Kb = 1,
        Mb = 2
    }
}
