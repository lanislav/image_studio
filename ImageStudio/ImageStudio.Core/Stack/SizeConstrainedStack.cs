﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImageStudio.Core.Commands;
using ImageStudio.Core.Utility;

namespace ImageStudio.Core.Stack
{
    /// <summary>
    /// Represents a size (memory) constrained stack.
    /// </summary>
    public class SizeConstrainedStack : ISizeConstrainedStack<ICommand>
    {
        private Stack<ICommand> _items;

        /// <summary>
        /// Gets or sets the items propery.
        /// </summary>
        public Stack<ICommand> Items
        {
            get => _items ?? new Stack<ICommand>();
            set => _items = value;
        }

        private double _sizeConstraintInMb;
        
        public event EventHandler<StackReshapeEventArgs> OnReshape;

        /// <summary>
        /// Gets or sets the size constraint in mega-bytes property.
        /// </summary>
        public double SizeConstraintInMb
        {
            get => _sizeConstraintInMb;
            set
            {
                CheckValue(value);

                _sizeConstraintInMb = value;

                CheckSizeConstraint();
            }
        }

        /// <summary>
        /// Gets the stack size mega-bytes.
        /// </summary>
        public double StackSizeInMb => Items.GetSizeInMegaBytes();

        /// <summary>
        /// Initializes a new instance of the <see cref="SizeConstrainedStack"/> class.
        /// </summary>
        public SizeConstrainedStack()
        {
            _sizeConstraintInMb = 0.0;

            _items = new Stack<ICommand>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SizeConstrainedStack"/> class with the specified size constraint.
        /// </summary>
        /// <param name="sizeConstraintInMb">The size constraint of the stack.</param>
        public SizeConstrainedStack(double sizeConstraintInMb) : this()
        {
            CheckValue(sizeConstraintInMb);

            _sizeConstraintInMb = sizeConstraintInMb;
        }

        /// <summary>
        /// Inserts an <see cref="ICommand"/> object at the top of the <see cref="SizeConstrainedStack"/>.
        /// </summary>
        /// <param name="command">The element that is being inserted.</param>
        public bool Push(ICommand command)
        {
            Items.Push(command);

            var result = CheckSizeConstraint();

            return result;
        }

        /// <summary>
        /// Removes an <see cref="ICommand"/> object from the top of the <see cref="SizeConstrainedStack"/> and returns it.
        /// </summary>
        /// <returns>An <see cref="ICommand"/> object.</returns>
        public ICommand Pop()
        {
            return Items.Any() ? Items.Pop() : null;
        }

        /// <summary>
        /// Returns the <see cref="ICommand"/> object from the top of the <see cref="SizeConstrainedStack"/>, 
        /// but does not alter the stack.
        /// </summary>
        /// <returns>An <see cref="ICommand"/> object.</returns>
        public ICommand Peek()
        {
            return Items.Any() ? Items.First() : null;
        }

        /// <summary>
        /// Removes all objects from the <see cref="SizeConstrainedStack"/>.
        /// </summary>
        public void Clear()
        {
            Items.Clear();
        }

        /// <summary>
        /// Determines whether there are any elements in the <see cref="SizeConstrainedStack"/>.
        /// </summary>
        /// <returns>True if there is at least one element in the stack.</returns>
        public bool Any()
        {
            return Items.Any();
        }

        /// <summary>
        /// Resizes the <see cref="SizeConstrainedStack"/> to fit the size constraint.
        /// </summary>
        public void ResizeToFitSizeConstraint()
        {
            var tempStack = new Stack<ICommand>();

            while (tempStack.GetSizeInMegaBytes() < SizeConstraintInMb)
            {
                if (tempStack.GetSizeInMegaBytes() + Items.Peek().UnExecute().GetSizeInMegaBytes() > SizeConstraintInMb)
                {
                    break;
                }
                tempStack.Push(Items.Pop());
            }

            tempStack = new Stack<ICommand>(tempStack.Reverse());

            var removedItemsCount = Items.Count;

            Items = tempStack;

            OnReshape?.Invoke(null, new StackReshapeEventArgs(removedItemsCount));
        }

        /// <summary>
        /// Checks if the size constraint is met, otherwise resizes the <see cref="SizeConstrainedStack"/> to meet the constraint. 
        /// </summary>
        private bool CheckSizeConstraint()
        {
            if (StackSizeInMb < SizeConstraintInMb)
            {
                return true;
            }

            ResizeToFitSizeConstraint();

            return false;
        }

        /// <summary>
        /// Checks if the specified value is valid.
        /// </summary>
        /// <param name="value">The value that is being checked.</param>
        private void CheckValue(double value)
        {
            if (value < 0)
            {
                throw new ArgumentException("Value can not be less than 0.", nameof(value));
            }
        }
    }
}
