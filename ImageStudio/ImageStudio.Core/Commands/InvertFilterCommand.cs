﻿using System.Drawing;
using ImageStudio.Core.Filters;

namespace ImageStudio.Core.Commands
{
    /// <summary>
    /// Implements the <see cref="ICommand"/> interface for the <see cref="InvertFilter"/>.
    /// </summary>
    public class InvertFilterCommand : ICommand
    {
        private readonly Image _image;
        private readonly bool _safeMode;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvertFilterCommand"/> class.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> the command will be executed on.</param>
        /// <param name="safeMode">Whether the command runs in safe mode or not.</param>
        public InvertFilterCommand(Image image, bool safeMode)
        {
            _image = image;
            _safeMode = safeMode;
        }

        /// <summary>
        /// Gets the name property.
        /// </summary>
        public string Name => "Invert";

        /// <summary>
        /// Executes the filter.
        /// </summary>
        /// <returns>The filtered image.</returns>
        public Image Execute()
        {
            return _safeMode ? InvertFilter.Invert(_image) : InvertFilter.InvertUnsafe(_image);
        }

        /// <summary>
        /// Unexecutes the filter.
        /// </summary>
        /// <returns>The original image.</returns>
        public Image UnExecute()
        {
            return _image;
        }
    }
}
