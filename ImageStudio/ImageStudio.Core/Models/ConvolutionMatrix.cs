﻿using System;

namespace ImageStudio.Core.Models
{
    public class ConvolutionMatrix
    {
        public int Size
        {
            get => _size;
            set
            {
                if (value % 2 == 0)
                {
                    throw new ArgumentException("The size of the matrix can not be an even number", nameof(value));
                }

                _size = value;
            }
        }

        /// <summary>
        /// Gets or sets the top-left property.
        /// </summary>
        public int TopLeft
        {
            get => this[0, 0];
            set => this[0, 0] = value;
        }

        /// <summary>
        /// Gets or sets the top-mid property.
        /// </summary>
        public int TopMid
        {
            get => this[0, 1];
            set => this[0, 1] = value;
        }

        /// <summary>
        /// Gets or sets the top-right property.
        /// </summary>
        public int TopRight
        {
            get => this[0, 2];
            set => this[0, 2] = value;
        }

        /// <summary>
        /// Gets or sets the mid-left property.
        /// </summary>
        public int MidLeft
        {
            get => this[1, 0];
            set => this[1, 0] = value;
        }

        /// <summary>
        /// Gets or sets the pixel property.
        /// </summary>
        public int Pixel
        {
            get => this[1, 1];
            set => this[1, 1] = value;
        }

        /// <summary>
        /// Gets or sets the mid-right property.
        /// </summary>
        public int MidRight
        {
            get => this[1, 2];
            set => this[1, 2] = value;
        }

        /// <summary>
        /// Gets or sets the bottom-left property.
        /// </summary>
        public int BottomLeft
        {
            get => this[2, 0];
            set => this[2, 0] = value;
        }

        /// <summary>
        /// Gets or sets the bottom-mid property.
        /// </summary>
        public int BottomMid
        {
            get => this[2, 1];
            set => this[2, 1] = value;
        }

        /// <summary>
        /// Gets or sets the bottom-right property.
        /// </summary>
        public int BottomRight
        {
            get => this[2, 2];
            set => this[2, 2] = value;
        }

        /// <summary>
        /// Gets or sets the factor property.
        /// </summary>
        public int Factor { get; set; }


        /// <summary>
        /// Gets or sets the offset property.
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Gets or sets the internal matrix field.
        /// </summary>
        public int[][] Matrix
        {
            get => _matrix;
            set
            {
                if (value.Length != Size)
                {
                    throw new ArgumentException("The size of the convoltuion matrix is not valid.", nameof(value));
                }

                _matrix = value;
            }
        }

        /// <summary>
        /// Gets or sets the value at the specified poisition.
        /// </summary>
        /// <param name="i">The row index.</param>
        /// <param name="j">The column index.</param>
        /// <returns>The value at the specified index.</returns>
        public int this[int i, int j]
        {
            get
            {
                CheckIndex(i);
                CheckIndex(j);

                return _matrix[i][j];
            }
            set
            {
                CheckIndex(i);
                CheckIndex(j);

                _matrix[i][j] = value;
            }
        }
        
        /// <summary>
        /// The internal matrix field.
        /// </summary>
        private int[][] _matrix;
        
        /// <summary>
        /// The size of the matrix;
        /// </summary>
        private int _size;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConvolutionMatrix"/> class.
        /// </summary>
        public ConvolutionMatrix()
        {
            _size = 3;

            _matrix = new int[Size][];

            for (var i = 0; i < Size; i++)
            {
                _matrix[i] = new int[Size];
            }

            LoadIdentity();
        }

        /// <summary>
        /// Loads the identity matrix.
        /// </summary>
        public void LoadIdentity()
        {
            SetAllMatrixValues(0);
            Pixel = Factor = 1;
            Offset = 0;
        }

        /// <summary>
        /// Sets all the matrix values to the specified value.
        /// </summary>
        /// <param name="value">The value that is being set.</param>
        public void SetAllMatrixValues(int value)
        {
            TopLeft = TopMid = TopRight = MidLeft = Pixel = MidRight = BottomLeft = BottomMid = BottomRight = value;
        }

        /// <summary>
        /// Checks if the specified index is within bounds of the matrix.
        /// </summary>
        /// <param name="index">The index being checked.</param>
        private void CheckIndex(int index)
        {
            if (index < 0 || index > Size - 1)
            {
                throw new ArgumentOutOfRangeException(nameof(index), "Index is outside the bounds of the matrix.");
            }
        }
    }
}
