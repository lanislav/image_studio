﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace ImageStudio.Core.Filters
{
    /// <summary>
    /// Implements the gamma filter.
    /// </summary>
    public static class GammaFilter
    {
        /// <summary>
        /// Performs the gamma filter operation the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <param name="red">The value for red gamma.</param>
        /// <param name="green">The value for green gamma.</param>
        /// <param name="blue">The value for blue gamma.</param>
        /// <returns>The filtered image.</returns>
        public static Image Gamma(Image image, double red, double green, double blue)
        {
            CheckValue(red);
            CheckValue(green);
            CheckValue(blue);

            var redGamma = new byte[256];
            var greenGamma = new byte[256];
            var blueGamma = new byte[256];

            for (var i = 0; i < 256; ++i)
            {
                redGamma[i] = (byte)Math.Min(255, (int)(255.0 * Math.Pow(i / 255.0, 1.0 / red) + 0.5));
                greenGamma[i] = (byte)Math.Min(255, (int)(255.0 * Math.Pow(i / 255.0, 1.0 / green) + 0.5));
                blueGamma[i] = (byte)Math.Min(255, (int)(255.0 * Math.Pow(i / 255.0, 1.0 / blue) + 0.5));
            }

            using (var bmp = new Bitmap(image))
            {
                for (var y = 0; y < bmp.Height; ++y)
                {
                    for (var x = 0; x < bmp.Width; ++x)
                    {
                        var color = bmp.GetPixel(x, y);

                        var redValue = redGamma[color.R];
                        var greenValue = redGamma[color.G];
                        var blueValue = redGamma[color.B];

                        bmp.SetPixel(x, y, Color.FromArgb(redValue, greenValue, blueValue));
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Performs the gamma filter operation the specified <see cref="Image"/>, but is unsafe.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <param name="red">The value for red gamma.</param>
        /// <param name="green">The value for green gamma.</param>
        /// <param name="blue">The value for blue gamma.</param>
        /// <returns>The filtered image.</returns>
        public static Image GammaUnsafe(Image image, double red, double green, double blue)
        {
            CheckValue(red);
            CheckValue(green);
            CheckValue(blue);

            var redGamma = new byte[256];
            var greenGamma = new byte[256];
            var blueGamma = new byte[256];

            for (var i = 0; i < 256; ++i)
            {
                redGamma[i] = (byte)Math.Min(255, (int)(255.0 * Math.Pow(i / 255.0, 1.0 / red) + 0.5));
                greenGamma[i] = (byte)Math.Min(255, (int)(255.0 * Math.Pow(i / 255.0, 1.0 / green) + 0.5));
                blueGamma[i] = (byte)Math.Min(255, (int)(255.0 * Math.Pow(i / 255.0, 1.0 / blue) + 0.5));
            }

            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    var p = (byte*)(void*)scan0;

                    var offset = stride - bmp.Width * 3;

                    for (var y = 0; y < bmp.Height; ++y)
                    {
                        for (var x = 0; x < bmp.Width; ++x)
                        {
                            p[2] = redGamma[p[2]];
                            p[1] = greenGamma[p[1]];
                            p[0] = blueGamma[p[0]];

                            p += 3;
                        }
                        p += offset;
                    }
                }

                bmp.UnlockBits(bmData);

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Checks if the specified value is a valid gamma value.
        /// </summary>
        /// <param name="value">The value that is being checked.</param>
        private static void CheckValue(double value)
        {
            if (value < .2 || value > 5)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "Specified value is out range.");
            }
        }
    }
}
