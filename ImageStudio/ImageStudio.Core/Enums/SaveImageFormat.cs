﻿namespace ImageStudio.Core.Enums
{
    public enum SaveImageFormat
    {
        Bmp = 1,
        Png = 2,
        Jpeg = 3
    }
}
