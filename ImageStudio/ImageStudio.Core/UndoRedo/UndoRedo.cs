﻿using System;
using System.Drawing;
using ImageStudio.Core.Commands;
using ImageStudio.Core.Stack;

namespace ImageStudio.Core.UndoRedo
{
    /// <summary>
    /// Implements the undo-redo opeartion functionalities.
    /// </summary>
    public class UndoRedo
    {
        private readonly ISizeConstrainedStack<ICommand> _undoSizeConstrainedStack;
        private readonly ISizeConstrainedStack<ICommand> _redoSizeConstrainedStack;

        /// <summary>
        /// Sets the undo stack OnReshape event.
        /// </summary>
        public EventHandler<StackReshapeEventArgs> UndoSizeConstrainedStackOnReshape
        {
            set => _undoSizeConstrainedStack.OnReshape += value;
        }

        /// <summary>
        /// Sets the redo stack OnReshape event.
        /// </summary>
        public EventHandler<StackReshapeEventArgs> RedoSizeConstrainedStackOnReshape
        {
            set => _redoSizeConstrainedStack.OnReshape += value;
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="UndoRedo"/> class with the specified size constraint.
        /// </summary>
        /// <param name="sizeConstraintInMb">The size constraint.</param>
        public UndoRedo(double sizeConstraintInMb)
        {
            _undoSizeConstrainedStack = new SizeConstrainedStack(sizeConstraintInMb);
            _redoSizeConstrainedStack = new SizeConstrainedStack(sizeConstraintInMb);
        }

        /// <summary>
        /// Performs the undo operation to the specified depth. Returns the <see cref="Image"/> of the last <see cref="ICommand"/>.
        /// </summary>
        /// <param name="level">The depth of the undo operation.</param>
        /// <returns>A image object.</returns>
        public Image Undo(int level)
        {
            if (!_undoSizeConstrainedStack.Any())
            {
                return null;
            }

            ICommand command = null;

            for (var i = 0; i < level; i++)
            {

                command = _undoSizeConstrainedStack.Pop();

                _redoSizeConstrainedStack.Push(command);
            }

            return command?.UnExecute();
        }

        /// <summary>
        /// Performs the redo operation to the specified depth. Returns the <see cref="Image"/> of the last <see cref="ICommand"/>.
        /// </summary>
        /// <param name="level">The depth of the redo operation.</param>
        /// <returns>A image object.</returns>
        public Image Redo(int level)
        {
            if (!_redoSizeConstrainedStack.Any())
            {
                return null;
            }

            ICommand command = null;

            for (var i = 0; i < level; i++)
            {

                command = _redoSizeConstrainedStack.Pop();

                _undoSizeConstrainedStack.Push(command);
            }

            return command?.Execute();
        }

        /// <summary>
        /// Inserts a new <see cref="ICommand"/> object to the <see cref="UndoRedo"/> internal stack.
        /// </summary>
        /// <param name="command">The <see cref="ICommand"/> that is being inserted.</param>
        public bool InsertCommand(ICommand command)
        {
            var result = _undoSizeConstrainedStack.Push(command);

            _redoSizeConstrainedStack.Clear();

            return result;
        }

        /// <summary>
        /// Sets the size constraint of the <see cref="UndoRedo"/> internal stack.
        /// </summary>
        /// <param name="stackSizeInMb">The size constraint in mega-bytes.</param>
        public void SetUndoRedoStackConstraint(double stackSizeInMb)
        {
            _undoSizeConstrainedStack.SizeConstraintInMb = stackSizeInMb;
            _redoSizeConstrainedStack.SizeConstraintInMb = stackSizeInMb;
        }

        /// <summary>
        /// Clears the <see cref="UndoRedo"/> object internal stack.
        /// </summary>
        public void Clear()
        {
            _undoSizeConstrainedStack.Clear();
            _redoSizeConstrainedStack.Clear();
        }
    }
}
