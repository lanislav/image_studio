﻿using System.Drawing;
using ImageStudio.Core.Filters;

namespace ImageStudio.Core.Commands
{
    public class SimpleColorizeCommand : ICommand
    {
        private readonly Image _image;
        private readonly Image _similarImage;
        private readonly bool _safeMode;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvertFilterCommand"/> class.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> the command will be executed on.</param>
        /// <param name="safeMode">Whether the command runs in safe mode or not.</param>
        /// <param name="similarImage"></param>
        public SimpleColorizeCommand(Image image, bool safeMode, Image similarImage = null)
        {
            _image = image;
            _safeMode = safeMode;
            _similarImage = similarImage;
        }

        /// <summary>
        /// Gets the name property.
        /// </summary>
        public string Name => "Simple Colorize";

        /// <summary>
        /// Executes the filter.
        /// </summary>
        /// <returns>The filtered image.</returns>
        public Image Execute()
        {
            return _safeMode ? SimpleColorizeFilter.Colorize(_image, _similarImage) : SimpleColorizeFilter.ColorizeUnsafe(_image, _similarImage);
        }

        /// <summary>
        /// Unexecutes the filter.
        /// </summary>
        /// <returns>The original image.</returns>
        public Image UnExecute()
        {
            return _image;
        }
    }
}
