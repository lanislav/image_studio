﻿using System;
using System.Drawing;
using System.IO;

namespace ImageStudio.Core.Utility
{
    /// <summary>
    /// Supports basic <see cref="Image"/> transformation operations.
    /// </summary>
    public static class ImageTransformation
    {
        /// <summary>
        /// Resizes the <see cref="Image"/> to a fixed width and height.
        /// </summary>
        /// <param name="imageStream">The <see cref="Stream"/> representing the image.</param>
        /// <param name="width">The width of the new image.</param>
        /// <param name="height">The height of the new image.</param>
        /// <returns>A new <see cref="Image"/> object of the specified size.</returns>
        public static Image Resize(Stream imageStream, int width, int height)
        {
            using (var img = Image.FromStream(imageStream))
            {
                int newWidth;

                var newHeight = (int) Math.Round(img.Height * width / (double) img.Width);

                if (newHeight <= height)
                {
                    newWidth = width;
                }
                else
                {
                    newWidth = (int) Math.Round(img.Width * height / (double) img.Height);
                    newHeight = height;
                }

                var resizedImage = new Bitmap(img, newWidth, newHeight);

                return resizedImage;
            }
        }
    }
}
