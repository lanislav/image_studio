﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace ImageStudio.Core.Filters
{
    /// <summary>
    /// Implements the edge detect difference filter.
    /// </summary>
    public static class EdgeDetectDifferenceFilter
    {
        /// <summary>
        /// Performs the edge detect difference filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> being filtered.</param>
        /// <param name="threshold">The threshold parameter.</param>
        /// <returns>The filtered image.</returns>
        public static Image EdgeDetectDifference(Image image, int threshold)
        {
            CheckThreshold(threshold);

            using (var bmp = new Bitmap(image))
            using (var bmpCloned = (Bitmap) bmp.Clone())
            {
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                    ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                var bmpDataCloned = bmpCloned.LockBits(new Rectangle(0, 0, bmpCloned.Width, bmpCloned.Height),
                    ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                
                var stride = bmpData.Stride;

                var bmpScan0 = bmpData.Scan0;
                var bmpClonedScan0 = bmpDataCloned.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)bmpScan0;
                        var pCloned = (byte*)(void*)bmpClonedScan0;

                        var nOffset = stride - bmp.Width * 3;
                        var nWidth = bmp.Width * 3;

                        int nPixel, nPixelMax;

                        p += stride;
                        pCloned += stride;

                        for (var y = 1; y < bmp.Height - 1; ++y)
                        {
                            p += 3;
                            pCloned += 3;

                            for (var x = 3; x < nWidth - 3; ++x)
                            {
                                nPixelMax = Math.Abs((pCloned - stride + 3)[0] - (pCloned + stride - 3)[0]);

                                nPixel = Math.Abs((pCloned + stride + 3)[0] - (pCloned - stride - 3)[0]);

                                if (nPixel > nPixelMax) nPixelMax = nPixel;

                                nPixel = Math.Abs((pCloned - stride)[0] - (pCloned + stride)[0]);

                                if (nPixel > nPixelMax) nPixelMax = nPixel;

                                nPixel = Math.Abs((pCloned + 3)[0] - (pCloned - 3)[0]);

                                if (nPixel > nPixelMax) nPixelMax = nPixel;

                                if (nPixelMax < threshold) nPixelMax = 0;

                                p[0] = (byte)nPixelMax;

                                ++p;
                                ++pCloned;
                            }

                            p += 3 + nOffset;
                            pCloned += 3 + nOffset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmpData);
                        bmpCloned.UnlockBits(bmpDataCloned);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Checks if the threshold is within the valid range.
        /// </summary>
        /// <param name="threshold">The value being checked.</param>
        public static void CheckThreshold(int threshold)
        {
            if (threshold < 0 || threshold > 255)
            {
                throw new ArgumentException("Threshold value is out of allowed range.", nameof(threshold));
            }
        }
    }
}
