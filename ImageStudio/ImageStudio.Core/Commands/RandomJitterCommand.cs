﻿using System.Drawing;
using ImageStudio.Core.Filters;

namespace ImageStudio.Core.Commands
{
    /// <summary>
    /// Implements the <see cref="ICommand"/> interface for the <see cref="RandomJitterFilter"/>.
    /// </summary>
    public class RandomJitterCommand : ICommand
    {
        private readonly Image _image;
        private readonly int _degree;

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomJitterCommand"/> class.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> the command will be executed on.</param>
        /// <param name="degree"></param>
        public RandomJitterCommand(Image image, int degree)
        {
            _image = image;
            _degree = degree;
        }

        /// <summary>
        /// Gets the name property.
        /// </summary>
        public string Name => "Random Jitter";

        /// <summary>
        /// Executes the filter.
        /// </summary>
        /// <returns>The filtered image.</returns>
        public Image Execute()
        {
            return RandomJitterFilter.RandomJitter(_image, _degree);
        }

        /// <summary>
        /// Unexecutes the filter.
        /// </summary>
        /// <returns>The original image.</returns>
        public Image UnExecute()
        {
            return _image;
        }
    }
}
