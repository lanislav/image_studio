﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using ImageStudio.Core.Enums;
using ImageStudio.Core.Stack;
using ImageStudio.GUI.Forms;
using ImageStudio.GUI.Presenters;
using ImageStudio.GUI.Views;

namespace ImageStudio.GUI
{
    public partial class MainForm : Form, IImageView
    {
        private readonly ImagePresenter _presenter;
        private Bitmap _bitmap;
        private ViewDisplayFormat _viewDisplayFormat = ViewDisplayFormat.OriginalImage;
        private double _redGamma = 1.0, _greenGamma = 1.0, _blueGamma = 1.0;
        private static double _sizeConstraint = 100.0;
        private static ConvolutionFilterMode _filterMode = ConvolutionFilterMode.NewImage;

        public MainForm()
        {
            InitializeComponent();

            _presenter = new ImagePresenter(this);
            
            InitializeComponentSettings();
        }

        #region IImageView implemenation 

        public Bitmap Bitmap
        {
            get => _bitmap;
            set
            {
                _bitmap = value;

                panelImgBackground.Invalidate();
            }
        }

        public ViewDisplayFormat ViewDisplayFormat
        {
            get => _viewDisplayFormat;
            set
            {
                _viewDisplayFormat = value;

                tabPageConvFilters.Enabled = value != ViewDisplayFormat.ConvolutionComparisonImages;
                
                panelImgBackground.Invalidate();
            }
        }

        public ConvolutionMatrixSize ConvolutionMatrixSize { get; set; } = ConvolutionMatrixSize.Conv3X3;

        public int SaveFileFormatIndex => saveFileDialog.FilterIndex;

        public bool IsSafeMode => safeModeToolStripMenuItem.Checked;

        public bool IsConvolutionFilterInplace => _filterMode == ConvolutionFilterMode.Inplace;

        public double RedGamma
        {
            get => Math.Max(0.2, _redGamma);
            set => _redGamma = value; 
        }

        public double GreenGamma
        {
            get => Math.Max(0.2, _greenGamma);
            set => _greenGamma = value;
        }

        public double BlueGamma
        {
            get => Math.Max(0.2, _blueGamma);
            set => _blueGamma = value;
        }

        public int RandomJitterDegree => trackBarRndJitter.Value;

        public int RedHistogramMin => int.TryParse(txtRMin.Text, out var val) ? val : 0;
        public int RedHistogramMax => int.TryParse(txtRMax.Text, out var val) ? val : 255;
        public int GreenHistogramMin => int.TryParse(txtGMin.Text, out var val) ? val : 0;
        public int GreenHistogramMax => int.TryParse(txtGMax.Text, out var val) ? val : 255;
        public int BlueHistogramMin => int.TryParse(txtBMin.Text, out var val) ? val : 0;
        public int BlueHistogramMax => int.TryParse(txtBMax.Text, out var val) ? val : 255;
        public int KuwaharaSize => int.TryParse(numKuwaharaSize.Text, out var val) ? val : 0;
        public int PointX { get; set; }
        public int PointY { get; set; }
        public Color Color => clrDialog.Color;
        public double SimilarityTreshold => double.TryParse(txtSimThreshold.Text, out var val) ? val : 0.0;

        public double Hue => double.TryParse(txtHue.Text, out var value) ? value : 0;

        public double? Saturation => double.TryParse(txtSaturation.Text, out var value)
            ? value > -1.0 && value < 5.0 ? (double?)value : null
            : null;

        public string LoadImage()
        {
            var dialogResult = openFileDialog.ShowDialog();

            return dialogResult == DialogResult.OK ? openFileDialog.FileName : string.Empty;
        }

        public string SaveImage()
        {
            if (Bitmap == null)
            {
                MessageBox.Show(@"First load an image in order to save it.", @"No image selected", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return string.Empty;
            }

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult != DialogResult.OK || string.IsNullOrWhiteSpace(saveFileDialog.FileName))
            {
                return string.Empty;
            }

            return saveFileDialog.FileName;
        }

        public void InsertUndoItem(string caption)
        {
            if (string.IsNullOrWhiteSpace(caption))
            {
                throw new ArgumentException(@"Caption can not be null or empty.", nameof(caption));
            }

            InsertMenuDropdownItem(undoToolStripMenuItem, UndoMenuItem_Click, caption, "Undo");
        }

        public void InsertRedoItem(string caption)
        {
            if (string.IsNullOrWhiteSpace(caption))
            {
                throw new ArgumentException(@"Caption can not be null or empty.", nameof(caption));
            }

            InsertMenuDropdownItem(redoToolStripMenuItem, RedoMenuItem_Click, caption, "Redo");
        }

        public void ClearRedoItems()
        {
            redoToolStripMenuItem.Enabled = false;
            redoToolStripMenuItem.DropDownItems.Clear();
        }

        public void ClearUndoRedoItems()
        {
            undoToolStripMenuItem.Enabled = false;
            undoToolStripMenuItem.DropDownItems.Clear();
            ClearRedoItems();
        }

        public void UndoStackOnReshapeHandler(object sender, StackReshapeEventArgs e)
        {
            UpdateMenuItemDropdown(undoToolStripMenuItem, e, "Undo");
        }

        public void RedoStackOnReshapeHandler(object sender, StackReshapeEventArgs e)
        {
            UpdateMenuItemDropdown(redoToolStripMenuItem, e, "Redo");
        }

        public double GetUndoRedoStackConstraint()
        {
            var form = new SettingsForm(_sizeConstraint, _filterMode);

            if (form.ShowDialog() != DialogResult.OK)
            {
                return -1.0;
            }

            _sizeConstraint = form.SizeConstraint;
            _filterMode = form.FilterMode;
            
            form.Dispose();

            return _sizeConstraint;
        }

        public void TerminateView()
        {
            Close();
        }

        #endregion

        #region Event handlers

        private void OpenFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _presenter.LoadImage();
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _presenter.SaveImage();
        }

        private void ClearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _presenter.ClearImage();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _presenter.Close();
        }

        private void SettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _presenter.SetUndoRedoStackConstraint();
        }

        private void ImageBackgroundPanelOnPaint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

            _presenter.DrawImage(e.Graphics);
        }

        private void OriginalImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewDisplayDropdownItemsCheckedStateHandler(sender, e);

            ViewDisplayFormat = ViewDisplayFormat.OriginalImage;
        }

        private void ChannelImagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewDisplayDropdownItemsCheckedStateHandler(sender, e);

            ViewDisplayFormat = ViewDisplayFormat.ChannelImages;
        }

        private void ConvolutionComparisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewDisplayDropdownItemsCheckedStateHandler(sender, e);

            ViewDisplayFormat = ViewDisplayFormat.ConvolutionComparisonImages;
        }

        private void ChannelHistogramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewDisplayDropdownItemsCheckedStateHandler(sender, e);

            ViewDisplayFormat = ViewDisplayFormat.ChannelHistogram;
        }

        private void GrayscaleComparisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewDisplayDropdownItemsCheckedStateHandler(sender, e);

            ViewDisplayFormat = ViewDisplayFormat.GrayscaleComparisonImages;
        }

        private void downsampledImagsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewDisplayDropdownItemsCheckedStateHandler(sender, e);

            ViewDisplayFormat = ViewDisplayFormat.DownsampleImages;
        }

        private void ViewDisplayDropdownItemsCheckedStateHandler(object sender, EventArgs e)
        {
            if (!(sender is ToolStripMenuItem) || ((ToolStripMenuItem) sender).Checked)
            {
                return;
            }

            var menuItem = (ToolStripMenuItem)sender;

            menuItem.Checked = true;

            for (var i = 0; i < viewToolStripMenuItem.DropDownItems.Count; i++)
            {
                if (viewToolStripMenuItem.DropDownItems[i].Text == menuItem.Text)
                {
                    continue;
                }

                ((ToolStripMenuItem) viewToolStripMenuItem.DropDownItems[i]).Checked = false;
            }
        }

        private void SafeModeToolStripMeuItem_Click(object sender, EventArgs e)
        {
            ModeOnChangeEventHandler(sender, unsafeModeToolStripMenuItem);
        }

        private void UnsafeModeToolStripMeuItem_Click(object sender, EventArgs e)
        {
            ModeOnChangeEventHandler(sender, safeModeToolStripMenuItem);
        }

        private static void ModeOnChangeEventHandler(object sender, object target)
        {
            var menuItem = (ToolStripMenuItem) sender;

            if (menuItem.Checked)
            {
                return;
            }

            menuItem.Checked = true;

            ((ToolStripMenuItem) target).Checked = false;
        }

        private void BtnColorPicker_Click(object sender, EventArgs e)
        {
            clrDialog.ShowDialog();
        }

        private void BtnInvert_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.Invert();
        }

        private void BtnGamma_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.Gamma();
        }

        private void BtnEmboss_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.EmbossLaplacian();
        }

        private void BtnEdgeDetect_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.EdgeDetectDifference();
        }

        private void BtnRandomJitter_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.RandomJitter();
        }


        private void BtnGrayscaleAvg_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.GrayscaleAvg();
        }

        private void BtnGrayscaleMax_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.GrayscaleMax();
        }

        private void BtnGrayscaleMinMaxMean_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.GrayscaleMinMaxMean();
        }

        private void BtnHistogramWindowFilter_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.Histogram();
        }

        private void BtnOrderedDither_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.OrderedDither();
        }

        private void btnStuckiDither_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.StuckiDither();
        }
        
        private void BtnSimpleColorize_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.SimpleColorize();
        }

        private void BtnSimilarColorize_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.SimpleColorizeSimilar();
        }

        private void BtnCrossDomainColorize_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.CrossDomainColoize();
        }

        private void BtnKuwahara_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull())
            {
                return;
            }

            _presenter.Kuwahara();
        }
        
        private void BtnEqualize_Click(object sender, EventArgs e)
        {
            if (CheckIfImageNull() || Color == Color.Empty || !_presenter.IsWithinBoundsOfImage(PointX, PointY))
            {
                return;
            }

            if (clrDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            _presenter.Equalize();
        }

        private void RadioBtn3x3_CheckedChanged(object sender, EventArgs e)
        {
            ConvolutionMatrixSize = ConvolutionMatrixSize.Conv3X3;
        }

        private void RadioBtn5x5_CheckedChanged(object sender, EventArgs e)
        {
            ConvolutionMatrixSize = ConvolutionMatrixSize.Conv5X5;
        }

        private void RadioBtn7x7_CheckedChanged(object sender, EventArgs e)
        {
            ConvolutionMatrixSize = ConvolutionMatrixSize.Conv7X7;
        }

        private void GammaRed_Scroll(object sender, EventArgs e)
        {
            var trackBar = (TrackBar) sender;
            var value = Math.Max(0.2, trackBar.Value / 20.0);
            RedGamma = value;
            tooltip.SetToolTip(trackBar, value.ToString(CultureInfo.InvariantCulture));
        }

        private void GammaGreen_Scroll(object sender, EventArgs e)
        {
            var trackBar = (TrackBar)sender;
            var value = Math.Max(0.2, trackBar.Value / 20.0);
            GreenGamma = value;
            tooltip.SetToolTip(trackBar, value.ToString(CultureInfo.InvariantCulture));
        }

        private void GammaBlue_Scroll(object sender, EventArgs e)
        {
            var trackBar = (TrackBar)sender;
            var value = Math.Max(0.2, trackBar.Value / 20.0);
            BlueGamma = value;
            tooltip.SetToolTip(trackBar, value.ToString(CultureInfo.InvariantCulture));
        }

        public void UndoMenuItem_Click(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;

            var level = menuItem.Tag ?? 0;

            _presenter.Undo((int)level);

            UpdateMenuItemDropdown(undoToolStripMenuItem, (int)level, "Undo", InsertRedoItem);
        }

        public void RedoMenuItem_Click(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;

            var level = menuItem.Tag ?? 0;

            _presenter.Redo((int)level);

            UpdateMenuItemDropdown(redoToolStripMenuItem, (int) level, "Redo", InsertUndoItem);
        }

        private void UpdateMenuItemDropdown(object sender, int level, string action, Action<string> callback)
        {
            if (!(sender is ToolStripMenuItem) || level <= 0 || ((ToolStripMenuItem)sender).DropDownItems.Count == 0)
            {
                return;
            }

            var menuItem = (ToolStripMenuItem) sender;

            for (var i = 0; i < level; i++)
            {
                var item = menuItem.DropDownItems[0];
                callback(item.Text);
                menuItem.DropDownItems.RemoveAt(0);
            }

            UpdateMenuItemDropdownItems(sender, action);
        }

        private void UpdateMenuItemDropdown(object sender, StackReshapeEventArgs e, string action)
        {
            if (!(sender is ToolStripMenuItem) || e.RemovedItemsCount <= 0 || ((ToolStripMenuItem) sender).DropDownItems.Count == 0)
            {
                return;
            }

            var menuItem = (ToolStripMenuItem)sender;

            for (var i = 0; i < e.RemovedItemsCount; i++)
            {
                menuItem.DropDownItems.RemoveAt(menuItem.DropDownItems.Count - 1);
            }

            UpdateMenuItemDropdownItems(sender, action);
        }

        private void InsertMenuDropdownItem(object sender, EventHandler eventHandler, string caption, string action)
        {
            var toolstripMenuItem = (ToolStripMenuItem)sender;

            if (toolstripMenuItem.DropDownItems.Count == 0)
            {
                toolstripMenuItem.Enabled = true;
            }

            var menuItem = new ToolStripMenuItem
            {
                Text = caption
            };
            menuItem.Click += eventHandler;

            toolstripMenuItem.DropDownItems.Insert(0, menuItem);

            UpdateMenuItemDropdownItems(sender, action);
        }

        private void UpdateMenuItemDropdownItems(object sender, string action)
        {
            var menuItem = (ToolStripMenuItem)sender;

            var list = menuItem.DropDownItems.Cast<ToolStripItem>().ToList();

            foreach (var item in list)
            {
                item.Tag = list.IndexOf(item) + 1;
                item.ToolTipText = $@"{action} {(int)item.Tag} Action{((int)item.Tag > 1 ? "s" : string.Empty)}";
            }

            menuItem.DropDownItems.AddRange(list.ToArray());

            if (menuItem.DropDownItems.Count == 0)
            {
                menuItem.Enabled = false;
            }
        }

        private void UndoToolStripMenuTopLevelItem_Click(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;

            if (!CheckMenuItemValid(menuItem))
            {
                return;
            }

            UndoMenuItem_Click(menuItem.DropDownItems.Cast<ToolStripItem>().First(), null);
        }

        private void RedoToolStripMenuTopLevelItem_Click(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;

            if (!CheckMenuItemValid(menuItem))
            {
                return;
            }

            RedoMenuItem_Click(menuItem.DropDownItems.Cast<ToolStripItem>().First(), null);
        }

        private void ToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            var menuItem = (ToolStripMenuItem)sender;

            if (!CheckMenuItemValid(menuItem))
            {
                return;
            }

            menuItem.ShowDropDown();
        }

        private static bool CheckMenuItemValid(ToolStripMenuItem menuItem)
        {
            return menuItem.Enabled && menuItem.DropDownItems.Count != 0;
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (!e.Control)
            {
                return;
            }

            switch (e.KeyCode)
            {
                case Keys.Z:
                    UndoToolStripMenuTopLevelItem_Click(undoToolStripMenuItem, null);
                    break;
                case Keys.Y:
                    RedoToolStripMenuTopLevelItem_Click(redoToolStripMenuItem, null);
                    break;
            }
        }

        private void TrackBarRandomJitter_Scroll(object sender, EventArgs e)
        {
            var trackBar = (TrackBar)sender;

            tooltip.SetToolTip(trackBar, trackBar.Value.ToString(CultureInfo.InvariantCulture));
        }

        private void TextBoxHistogramValues_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void TxtBoxSaturation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            var textBox = (TextBox)sender;

            if (e.KeyChar == '.' && textBox.Text.IndexOf('.') > -1 && textBox.Text.IndexOf('.') == textBox.Text.LastIndexOf('.'))
            {
                e.Handled = true;
            }
        }

        private void TxtBoxHue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != '-')
            {
                e.Handled = true;
            }

            var textBox = (TextBox)sender;

            switch (e.KeyChar)
            {
                case '.' when textBox.Text.IndexOf('.') > -1 && textBox.Text.IndexOf('.') == textBox.Text.LastIndexOf('.'):
                    e.Handled = true;
                    break;
                case '-' when textBox.Text.IndexOf('-') > -1 && textBox.Text.IndexOf('-') == 0 && textBox.Text.IndexOf('-') == textBox.Text.LastIndexOf('-'):
                    e.Handled = true;
                    break;
            }
        }

        private void PanelImgBackground_Click(object sender, EventArgs e)
        {
            var mouseArgs = (MouseEventArgs) e;

            if (_presenter.IsWithinBoundsOfImage(mouseArgs.X, mouseArgs.Y))
            {
                PointX = mouseArgs.X;
                PointY = mouseArgs.Y;
            }
        }

        #endregion

        #region Utility

        private void InitializeComponentSettings()
        {
            var initialFolder = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

            openFileDialog.Filter = ConfigurationManager.AppSettings["OpenFileDialogFilter"];
            openFileDialog.InitialDirectory = initialFolder;

            saveFileDialog.Filter = ConfigurationManager.AppSettings["SaveFileDialogFilter"];
            saveFileDialog.InitialDirectory = initialFolder;
            saveFileDialog.OverwritePrompt = true;

            undoToolStripMenuItem.DropDown.MaximumSize = new Size(200, 250);
            redoToolStripMenuItem.DropDown.MaximumSize = new Size(200, 250);
            
            _presenter.CalculateImageContainer(panelImgBackground.Width, panelImgBackground.Height);
        }

        private bool CheckIfImageNull()
        {
            var result = Bitmap == null;

            if (result)
            {
                MessageBox.Show(@"There is no image on the dashboard.", @"No image selected", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

            return result;
        }

        #endregion
    }
}
