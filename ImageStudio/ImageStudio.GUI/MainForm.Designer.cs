﻿namespace ImageStudio.GUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.originalImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.channelImagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convolutionComparisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.channelHistogramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grayscaleComparisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.safeModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unsafeModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.panelImgBackground = new System.Windows.Forms.Panel();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.btnInvert = new System.Windows.Forms.Button();
            this.lblInvert = new System.Windows.Forms.Label();
            this.tbGammaRed = new System.Windows.Forms.TrackBar();
            this.lblGamma = new System.Windows.Forms.Label();
            this.tbGammaGreen = new System.Windows.Forms.TrackBar();
            this.tbGammaBlue = new System.Windows.Forms.TrackBar();
            this.btnGamma = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.tabFilters1 = new System.Windows.Forms.TabControl();
            this.tabPageBasicFilters = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabPageConvFilters = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioBtn7x7 = new System.Windows.Forms.RadioButton();
            this.radioBtn5x5 = new System.Windows.Forms.RadioButton();
            this.radioBtn3x3 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEmboss = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.btnGrayscaleMinMaxMean = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnGrayscaleMax = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnGrayscaleAvg = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtBMax = new System.Windows.Forms.TextBox();
            this.txtBMin = new System.Windows.Forms.TextBox();
            this.txtGMax = new System.Windows.Forms.TextBox();
            this.txtGMin = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRMax = new System.Windows.Forms.TextBox();
            this.txtRMin = new System.Windows.Forms.TextBox();
            this.btnHistogramWindowFilter = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnRndJitter = new System.Windows.Forms.Button();
            this.btnEdgeDetectDiff = new System.Windows.Forms.Button();
            this.tabFilters2 = new System.Windows.Forms.TabControl();
            this.tabPageEdgeDetectFilters = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.numKuwaharaSize = new System.Windows.Forms.NumericUpDown();
            this.btnKuwahara = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPageOffsetFilters = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSimThreshold = new System.Windows.Forms.TextBox();
            this.btnEqualize = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.trackBarRndJitter = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnStuckiDither = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.btnOrderedDither = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSaturation = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtHue = new System.Windows.Forms.TextBox();
            this.btnCrossDomainColorize = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.btnSimilarColorize = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.btnSimpleColorize = new System.Windows.Forms.Button();
            this.clrDialog = new System.Windows.Forms.ColorDialog();
            this.downsampledImagsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbGammaRed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGammaGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGammaBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.tabFilters1.SuspendLayout();
            this.tabPageBasicFilters.SuspendLayout();
            this.tabPageConvFilters.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabFilters2.SuspendLayout();
            this.tabPageEdgeDetectFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numKuwaharaSize)).BeginInit();
            this.tabPageOffsetFilters.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRndJitter)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.modeToolStripMenuItem,
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip.ShowItemToolTips = true;
            this.menuStrip.Size = new System.Drawing.Size(1184, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.clearToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("fileToolStripMenuItem.Image")));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenFileToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("clearToolStripMenuItem.Image")));
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.ClearToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.originalImageToolStripMenuItem,
            this.channelImagesToolStripMenuItem,
            this.convolutionComparisonToolStripMenuItem,
            this.channelHistogramToolStripMenuItem,
            this.grayscaleComparisonToolStripMenuItem,
            this.downsampledImagsToolStripMenuItem});
            this.viewToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("viewToolStripMenuItem.Image")));
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // originalImageToolStripMenuItem
            // 
            this.originalImageToolStripMenuItem.Checked = true;
            this.originalImageToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.originalImageToolStripMenuItem.Name = "originalImageToolStripMenuItem";
            this.originalImageToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.originalImageToolStripMenuItem.Text = "Original image";
            this.originalImageToolStripMenuItem.Click += new System.EventHandler(this.OriginalImageToolStripMenuItem_Click);
            // 
            // channelImagesToolStripMenuItem
            // 
            this.channelImagesToolStripMenuItem.Name = "channelImagesToolStripMenuItem";
            this.channelImagesToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.channelImagesToolStripMenuItem.Text = "Channel images";
            this.channelImagesToolStripMenuItem.Click += new System.EventHandler(this.ChannelImagesToolStripMenuItem_Click);
            // 
            // convolutionComparisonToolStripMenuItem
            // 
            this.convolutionComparisonToolStripMenuItem.Name = "convolutionComparisonToolStripMenuItem";
            this.convolutionComparisonToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.convolutionComparisonToolStripMenuItem.Text = "Convolution comparison";
            this.convolutionComparisonToolStripMenuItem.Click += new System.EventHandler(this.ConvolutionComparisonToolStripMenuItem_Click);
            // 
            // channelHistogramToolStripMenuItem
            // 
            this.channelHistogramToolStripMenuItem.Name = "channelHistogramToolStripMenuItem";
            this.channelHistogramToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.channelHistogramToolStripMenuItem.Text = "Channel histogram";
            this.channelHistogramToolStripMenuItem.Click += new System.EventHandler(this.ChannelHistogramToolStripMenuItem_Click);
            // 
            // grayscaleComparisonToolStripMenuItem
            // 
            this.grayscaleComparisonToolStripMenuItem.Name = "grayscaleComparisonToolStripMenuItem";
            this.grayscaleComparisonToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.grayscaleComparisonToolStripMenuItem.Text = "Grayscale comparison";
            this.grayscaleComparisonToolStripMenuItem.Click += new System.EventHandler(this.GrayscaleComparisonToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("settingsToolStripMenuItem.Image")));
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.SettingsToolStripMenuItem_Click);
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modeToolStripMenuItem1});
            this.modeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("modeToolStripMenuItem.Image")));
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.modeToolStripMenuItem.Text = "Options";
            // 
            // modeToolStripMenuItem1
            // 
            this.modeToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.safeModeToolStripMenuItem,
            this.unsafeModeToolStripMenuItem});
            this.modeToolStripMenuItem1.Name = "modeToolStripMenuItem1";
            this.modeToolStripMenuItem1.Size = new System.Drawing.Size(105, 22);
            this.modeToolStripMenuItem1.Text = "Mode";
            // 
            // safeModeToolStripMenuItem
            // 
            this.safeModeToolStripMenuItem.Checked = true;
            this.safeModeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.safeModeToolStripMenuItem.Name = "safeModeToolStripMenuItem";
            this.safeModeToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.safeModeToolStripMenuItem.Text = "Safe";
            this.safeModeToolStripMenuItem.Click += new System.EventHandler(this.SafeModeToolStripMeuItem_Click);
            // 
            // unsafeModeToolStripMenuItem
            // 
            this.unsafeModeToolStripMenuItem.Name = "unsafeModeToolStripMenuItem";
            this.unsafeModeToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.unsafeModeToolStripMenuItem.Text = "Unsafe";
            this.unsafeModeToolStripMenuItem.Click += new System.EventHandler(this.UnsafeModeToolStripMeuItem_Click);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Enabled = false;
            this.undoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("undoToolStripMenuItem.Image")));
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.undoToolStripMenuItem.ShowShortcutKeys = false;
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(28, 20);
            this.undoToolStripMenuItem.Tag = "";
            this.undoToolStripMenuItem.ToolTipText = "Undo (Ctrl +Z)";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.UndoToolStripMenuTopLevelItem_Click);
            this.undoToolStripMenuItem.MouseEnter += new System.EventHandler(this.ToolStripMenuItem_MouseEnter);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Enabled = false;
            this.redoToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("redoToolStripMenuItem.Image")));
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.ShowShortcutKeys = false;
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(28, 20);
            this.redoToolStripMenuItem.Tag = "";
            this.redoToolStripMenuItem.ToolTipText = "Redo (Ctrl + Y)";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.RedoToolStripMenuTopLevelItem_Click);
            this.redoToolStripMenuItem.MouseEnter += new System.EventHandler(this.ToolStripMenuItem_MouseEnter);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Title = "Image Studio - Open Image";
            // 
            // panelImgBackground
            // 
            this.panelImgBackground.AutoScroll = true;
            this.panelImgBackground.BackColor = System.Drawing.Color.Silver;
            this.panelImgBackground.Location = new System.Drawing.Point(0, 24);
            this.panelImgBackground.Name = "panelImgBackground";
            this.panelImgBackground.Size = new System.Drawing.Size(900, 638);
            this.panelImgBackground.TabIndex = 1;
            this.panelImgBackground.Click += new System.EventHandler(this.PanelImgBackground_Click);
            this.panelImgBackground.Paint += new System.Windows.Forms.PaintEventHandler(this.ImageBackgroundPanelOnPaint);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Title = "Image Studio - Save Image";
            // 
            // btnInvert
            // 
            this.btnInvert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInvert.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInvert.Location = new System.Drawing.Point(176, 15);
            this.btnInvert.Name = "btnInvert";
            this.btnInvert.Size = new System.Drawing.Size(83, 31);
            this.btnInvert.TabIndex = 2;
            this.btnInvert.Text = "Apply";
            this.btnInvert.UseVisualStyleBackColor = true;
            this.btnInvert.Click += new System.EventHandler(this.BtnInvert_Click);
            // 
            // lblInvert
            // 
            this.lblInvert.AutoSize = true;
            this.lblInvert.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInvert.Location = new System.Drawing.Point(9, 13);
            this.lblInvert.Name = "lblInvert";
            this.lblInvert.Size = new System.Drawing.Size(77, 29);
            this.lblInvert.TabIndex = 3;
            this.lblInvert.Text = "Invert";
            // 
            // tbGammaRed
            // 
            this.tbGammaRed.BackColor = System.Drawing.Color.White;
            this.tbGammaRed.Location = new System.Drawing.Point(49, 105);
            this.tbGammaRed.Maximum = 100;
            this.tbGammaRed.Name = "tbGammaRed";
            this.tbGammaRed.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbGammaRed.Size = new System.Drawing.Size(223, 45);
            this.tbGammaRed.TabIndex = 7;
            this.tbGammaRed.TickFrequency = 5;
            this.tbGammaRed.Value = 20;
            this.tbGammaRed.Scroll += new System.EventHandler(this.GammaRed_Scroll);
            // 
            // lblGamma
            // 
            this.lblGamma.AutoSize = true;
            this.lblGamma.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGamma.Location = new System.Drawing.Point(9, 69);
            this.lblGamma.Name = "lblGamma";
            this.lblGamma.Size = new System.Drawing.Size(102, 29);
            this.lblGamma.TabIndex = 8;
            this.lblGamma.Text = "Gamma";
            // 
            // tbGammaGreen
            // 
            this.tbGammaGreen.BackColor = System.Drawing.Color.White;
            this.tbGammaGreen.Location = new System.Drawing.Point(49, 150);
            this.tbGammaGreen.Maximum = 100;
            this.tbGammaGreen.Name = "tbGammaGreen";
            this.tbGammaGreen.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbGammaGreen.Size = new System.Drawing.Size(223, 45);
            this.tbGammaGreen.TabIndex = 9;
            this.tbGammaGreen.TickFrequency = 5;
            this.tbGammaGreen.Value = 20;
            this.tbGammaGreen.Scroll += new System.EventHandler(this.GammaGreen_Scroll);
            // 
            // tbGammaBlue
            // 
            this.tbGammaBlue.BackColor = System.Drawing.Color.White;
            this.tbGammaBlue.Location = new System.Drawing.Point(49, 195);
            this.tbGammaBlue.Maximum = 100;
            this.tbGammaBlue.Name = "tbGammaBlue";
            this.tbGammaBlue.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbGammaBlue.Size = new System.Drawing.Size(223, 45);
            this.tbGammaBlue.TabIndex = 10;
            this.tbGammaBlue.TickFrequency = 5;
            this.tbGammaBlue.Value = 20;
            this.tbGammaBlue.Scroll += new System.EventHandler(this.GammaBlue_Scroll);
            // 
            // btnGamma
            // 
            this.btnGamma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGamma.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGamma.Location = new System.Drawing.Point(176, 246);
            this.btnGamma.Name = "btnGamma";
            this.btnGamma.Size = new System.Drawing.Size(83, 31);
            this.btnGamma.TabIndex = 11;
            this.btnGamma.Text = "Apply";
            this.btnGamma.UseVisualStyleBackColor = true;
            this.btnGamma.Click += new System.EventHandler(this.BtnGamma_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(14, 105);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(30, 30);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(14, 150);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(30, 30);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 13;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(14, 195);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(30, 30);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 14;
            this.pictureBox6.TabStop = false;
            // 
            // tabFilters1
            // 
            this.tabFilters1.Controls.Add(this.tabPageBasicFilters);
            this.tabFilters1.Controls.Add(this.tabPageConvFilters);
            this.tabFilters1.Controls.Add(this.tabPage1);
            this.tabFilters1.Controls.Add(this.tabPage2);
            this.tabFilters1.Location = new System.Drawing.Point(900, 24);
            this.tabFilters1.Name = "tabFilters1";
            this.tabFilters1.SelectedIndex = 0;
            this.tabFilters1.Size = new System.Drawing.Size(286, 320);
            this.tabFilters1.TabIndex = 0;
            // 
            // tabPageBasicFilters
            // 
            this.tabPageBasicFilters.Controls.Add(this.panel1);
            this.tabPageBasicFilters.Controls.Add(this.pictureBox6);
            this.tabPageBasicFilters.Controls.Add(this.lblInvert);
            this.tabPageBasicFilters.Controls.Add(this.pictureBox5);
            this.tabPageBasicFilters.Controls.Add(this.btnInvert);
            this.tabPageBasicFilters.Controls.Add(this.pictureBox4);
            this.tabPageBasicFilters.Controls.Add(this.btnGamma);
            this.tabPageBasicFilters.Controls.Add(this.tbGammaBlue);
            this.tabPageBasicFilters.Controls.Add(this.tbGammaRed);
            this.tabPageBasicFilters.Controls.Add(this.tbGammaGreen);
            this.tabPageBasicFilters.Controls.Add(this.lblGamma);
            this.tabPageBasicFilters.Location = new System.Drawing.Point(4, 22);
            this.tabPageBasicFilters.Name = "tabPageBasicFilters";
            this.tabPageBasicFilters.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageBasicFilters.Size = new System.Drawing.Size(278, 294);
            this.tabPageBasicFilters.TabIndex = 0;
            this.tabPageBasicFilters.Text = "Basic filters";
            this.tabPageBasicFilters.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(4, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(270, 1);
            this.panel1.TabIndex = 15;
            // 
            // tabPageConvFilters
            // 
            this.tabPageConvFilters.Controls.Add(this.groupBox1);
            this.tabPageConvFilters.Controls.Add(this.label1);
            this.tabPageConvFilters.Controls.Add(this.btnEmboss);
            this.tabPageConvFilters.Location = new System.Drawing.Point(4, 22);
            this.tabPageConvFilters.Name = "tabPageConvFilters";
            this.tabPageConvFilters.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageConvFilters.Size = new System.Drawing.Size(278, 294);
            this.tabPageConvFilters.TabIndex = 1;
            this.tabPageConvFilters.Text = "Convolution filters";
            this.tabPageConvFilters.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioBtn7x7);
            this.groupBox1.Controls.Add(this.radioBtn5x5);
            this.groupBox1.Controls.Add(this.radioBtn3x3);
            this.groupBox1.Location = new System.Drawing.Point(11, 45);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(257, 119);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Convolution matrix size";
            // 
            // radioBtn7x7
            // 
            this.radioBtn7x7.AutoSize = true;
            this.radioBtn7x7.Location = new System.Drawing.Point(22, 78);
            this.radioBtn7x7.Name = "radioBtn7x7";
            this.radioBtn7x7.Size = new System.Drawing.Size(44, 17);
            this.radioBtn7x7.TabIndex = 2;
            this.radioBtn7x7.Text = "7X7";
            this.radioBtn7x7.UseVisualStyleBackColor = true;
            this.radioBtn7x7.CheckedChanged += new System.EventHandler(this.RadioBtn7x7_CheckedChanged);
            // 
            // radioBtn5x5
            // 
            this.radioBtn5x5.AutoSize = true;
            this.radioBtn5x5.Location = new System.Drawing.Point(22, 55);
            this.radioBtn5x5.Name = "radioBtn5x5";
            this.radioBtn5x5.Size = new System.Drawing.Size(44, 17);
            this.radioBtn5x5.TabIndex = 1;
            this.radioBtn5x5.Text = "5X5";
            this.radioBtn5x5.UseVisualStyleBackColor = true;
            this.radioBtn5x5.CheckedChanged += new System.EventHandler(this.RadioBtn5x5_CheckedChanged);
            // 
            // radioBtn3x3
            // 
            this.radioBtn3x3.AutoSize = true;
            this.radioBtn3x3.Checked = true;
            this.radioBtn3x3.Location = new System.Drawing.Point(22, 32);
            this.radioBtn3x3.Name = "radioBtn3x3";
            this.radioBtn3x3.Size = new System.Drawing.Size(44, 17);
            this.radioBtn3x3.TabIndex = 0;
            this.radioBtn3x3.TabStop = true;
            this.radioBtn3x3.Text = "3X3";
            this.radioBtn3x3.UseVisualStyleBackColor = true;
            this.radioBtn3x3.CheckedChanged += new System.EventHandler(this.RadioBtn3x3_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Emboss Laplacian";
            // 
            // btnEmboss
            // 
            this.btnEmboss.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmboss.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmboss.Location = new System.Drawing.Point(176, 178);
            this.btnEmboss.Name = "btnEmboss";
            this.btnEmboss.Size = new System.Drawing.Size(83, 31);
            this.btnEmboss.TabIndex = 0;
            this.btnEmboss.Text = "Apply";
            this.btnEmboss.UseVisualStyleBackColor = true;
            this.btnEmboss.Click += new System.EventHandler(this.BtnEmboss_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.btnGrayscaleMinMaxMean);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.btnGrayscaleMax);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.btnGrayscaleAvg);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(278, 294);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Grayscale filters";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(4, 100);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(270, 1);
            this.panel3.TabIndex = 17;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(4, 51);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(270, 1);
            this.panel2.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 29);
            this.label6.TabIndex = 9;
            this.label6.Text = "(Min-Max)/2";
            // 
            // btnGrayscaleMinMaxMean
            // 
            this.btnGrayscaleMinMaxMean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrayscaleMinMaxMean.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrayscaleMinMaxMean.Location = new System.Drawing.Point(176, 113);
            this.btnGrayscaleMinMaxMean.Name = "btnGrayscaleMinMaxMean";
            this.btnGrayscaleMinMaxMean.Size = new System.Drawing.Size(83, 31);
            this.btnGrayscaleMinMaxMean.TabIndex = 8;
            this.btnGrayscaleMinMaxMean.Text = "Apply";
            this.btnGrayscaleMinMaxMean.UseVisualStyleBackColor = true;
            this.btnGrayscaleMinMaxMean.Click += new System.EventHandler(this.BtnGrayscaleMinMaxMean_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 29);
            this.label5.TabIndex = 7;
            this.label5.Text = "Max";
            // 
            // btnGrayscaleMax
            // 
            this.btnGrayscaleMax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrayscaleMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrayscaleMax.Location = new System.Drawing.Point(176, 61);
            this.btnGrayscaleMax.Name = "btnGrayscaleMax";
            this.btnGrayscaleMax.Size = new System.Drawing.Size(83, 31);
            this.btnGrayscaleMax.TabIndex = 6;
            this.btnGrayscaleMax.Text = "Apply";
            this.btnGrayscaleMax.UseVisualStyleBackColor = true;
            this.btnGrayscaleMax.Click += new System.EventHandler(this.BtnGrayscaleMax_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 29);
            this.label4.TabIndex = 5;
            this.label4.Text = "Avg";
            // 
            // btnGrayscaleAvg
            // 
            this.btnGrayscaleAvg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrayscaleAvg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGrayscaleAvg.Location = new System.Drawing.Point(176, 12);
            this.btnGrayscaleAvg.Name = "btnGrayscaleAvg";
            this.btnGrayscaleAvg.Size = new System.Drawing.Size(83, 31);
            this.btnGrayscaleAvg.TabIndex = 4;
            this.btnGrayscaleAvg.Text = "Apply";
            this.btnGrayscaleAvg.UseVisualStyleBackColor = true;
            this.btnGrayscaleAvg.Click += new System.EventHandler(this.BtnGrayscaleAvg_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtBMax);
            this.tabPage2.Controls.Add(this.txtBMin);
            this.tabPage2.Controls.Add(this.txtGMax);
            this.tabPage2.Controls.Add(this.txtGMin);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.txtRMax);
            this.tabPage2.Controls.Add(this.txtRMin);
            this.tabPage2.Controls.Add(this.btnHistogramWindowFilter);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(278, 294);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Histogram filters";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtBMax
            // 
            this.txtBMax.Location = new System.Drawing.Point(183, 112);
            this.txtBMax.Name = "txtBMax";
            this.txtBMax.Size = new System.Drawing.Size(73, 20);
            this.txtBMax.TabIndex = 16;
            // 
            // txtBMin
            // 
            this.txtBMin.Location = new System.Drawing.Point(104, 112);
            this.txtBMin.Name = "txtBMin";
            this.txtBMin.Size = new System.Drawing.Size(73, 20);
            this.txtBMin.TabIndex = 15;
            // 
            // txtGMax
            // 
            this.txtGMax.Location = new System.Drawing.Point(183, 83);
            this.txtGMax.Name = "txtGMax";
            this.txtGMax.Size = new System.Drawing.Size(73, 20);
            this.txtGMax.TabIndex = 14;
            // 
            // txtGMin
            // 
            this.txtGMin.Location = new System.Drawing.Point(104, 83);
            this.txtGMin.Name = "txtGMin";
            this.txtGMin.Size = new System.Drawing.Size(73, 20);
            this.txtGMin.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 20);
            this.label10.TabIndex = 12;
            this.label10.Text = "B channel:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 81);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 20);
            this.label9.TabIndex = 11;
            this.label9.Text = "G channel:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 20);
            this.label8.TabIndex = 10;
            this.label8.Text = "R channel:";
            // 
            // txtRMax
            // 
            this.txtRMax.Location = new System.Drawing.Point(183, 52);
            this.txtRMax.Name = "txtRMax";
            this.txtRMax.Size = new System.Drawing.Size(73, 20);
            this.txtRMax.TabIndex = 9;
            this.txtRMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxHistogramValues_KeyPress);
            // 
            // txtRMin
            // 
            this.txtRMin.Location = new System.Drawing.Point(104, 52);
            this.txtRMin.Name = "txtRMin";
            this.txtRMin.Size = new System.Drawing.Size(73, 20);
            this.txtRMin.TabIndex = 8;
            this.txtRMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxHistogramValues_KeyPress);
            // 
            // btnHistogramWindowFilter
            // 
            this.btnHistogramWindowFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHistogramWindowFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHistogramWindowFilter.Location = new System.Drawing.Point(180, 155);
            this.btnHistogramWindowFilter.Name = "btnHistogramWindowFilter";
            this.btnHistogramWindowFilter.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnHistogramWindowFilter.Size = new System.Drawing.Size(83, 31);
            this.btnHistogramWindowFilter.TabIndex = 6;
            this.btnHistogramWindowFilter.Text = "Apply";
            this.btnHistogramWindowFilter.UseVisualStyleBackColor = true;
            this.btnHistogramWindowFilter.Click += new System.EventHandler(this.BtnHistogramWindowFilter_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(254, 29);
            this.label7.TabIndex = 7;
            this.label7.Text = "Blue and green zone";
            // 
            // btnRndJitter
            // 
            this.btnRndJitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRndJitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRndJitter.Location = new System.Drawing.Point(176, 106);
            this.btnRndJitter.Name = "btnRndJitter";
            this.btnRndJitter.Size = new System.Drawing.Size(83, 31);
            this.btnRndJitter.TabIndex = 2;
            this.btnRndJitter.Text = "Apply";
            this.btnRndJitter.UseVisualStyleBackColor = true;
            this.btnRndJitter.Click += new System.EventHandler(this.BtnRandomJitter_Click);
            // 
            // btnEdgeDetectDiff
            // 
            this.btnEdgeDetectDiff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdgeDetectDiff.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdgeDetectDiff.Location = new System.Drawing.Point(176, 51);
            this.btnEdgeDetectDiff.Name = "btnEdgeDetectDiff";
            this.btnEdgeDetectDiff.Size = new System.Drawing.Size(83, 31);
            this.btnEdgeDetectDiff.TabIndex = 1;
            this.btnEdgeDetectDiff.Text = "Apply";
            this.btnEdgeDetectDiff.UseVisualStyleBackColor = true;
            this.btnEdgeDetectDiff.Click += new System.EventHandler(this.BtnEdgeDetect_Click);
            // 
            // tabFilters2
            // 
            this.tabFilters2.Controls.Add(this.tabPageEdgeDetectFilters);
            this.tabFilters2.Controls.Add(this.tabPageOffsetFilters);
            this.tabFilters2.Controls.Add(this.tabPage3);
            this.tabFilters2.Controls.Add(this.tabPage4);
            this.tabFilters2.Location = new System.Drawing.Point(900, 342);
            this.tabFilters2.Name = "tabFilters2";
            this.tabFilters2.SelectedIndex = 0;
            this.tabFilters2.Size = new System.Drawing.Size(286, 320);
            this.tabFilters2.TabIndex = 1;
            // 
            // tabPageEdgeDetectFilters
            // 
            this.tabPageEdgeDetectFilters.Controls.Add(this.label19);
            this.tabPageEdgeDetectFilters.Controls.Add(this.numKuwaharaSize);
            this.tabPageEdgeDetectFilters.Controls.Add(this.btnKuwahara);
            this.tabPageEdgeDetectFilters.Controls.Add(this.label18);
            this.tabPageEdgeDetectFilters.Controls.Add(this.btnEdgeDetectDiff);
            this.tabPageEdgeDetectFilters.Controls.Add(this.label2);
            this.tabPageEdgeDetectFilters.Location = new System.Drawing.Point(4, 22);
            this.tabPageEdgeDetectFilters.Name = "tabPageEdgeDetectFilters";
            this.tabPageEdgeDetectFilters.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageEdgeDetectFilters.Size = new System.Drawing.Size(278, 294);
            this.tabPageEdgeDetectFilters.TabIndex = 0;
            this.tabPageEdgeDetectFilters.Text = "Edge detect filters";
            this.tabPageEdgeDetectFilters.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(10, 131);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 20);
            this.label19.TabIndex = 11;
            this.label19.Text = "Size:";
            // 
            // numKuwaharaSize
            // 
            this.numKuwaharaSize.Location = new System.Drawing.Point(60, 134);
            this.numKuwaharaSize.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numKuwaharaSize.Name = "numKuwaharaSize";
            this.numKuwaharaSize.Size = new System.Drawing.Size(199, 20);
            this.numKuwaharaSize.TabIndex = 7;
            // 
            // btnKuwahara
            // 
            this.btnKuwahara.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnKuwahara.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKuwahara.Location = new System.Drawing.Point(176, 173);
            this.btnKuwahara.Name = "btnKuwahara";
            this.btnKuwahara.Size = new System.Drawing.Size(83, 31);
            this.btnKuwahara.TabIndex = 5;
            this.btnKuwahara.Text = "Apply";
            this.btnKuwahara.UseVisualStyleBackColor = true;
            this.btnKuwahara.Click += new System.EventHandler(this.BtnKuwahara_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(9, 99);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(128, 29);
            this.label18.TabIndex = 6;
            this.label18.Text = "Kuwahara";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 29);
            this.label2.TabIndex = 4;
            this.label2.Text = "E. D. Difference";
            // 
            // tabPageOffsetFilters
            // 
            this.tabPageOffsetFilters.Controls.Add(this.label21);
            this.tabPageOffsetFilters.Controls.Add(this.txtSimThreshold);
            this.tabPageOffsetFilters.Controls.Add(this.btnEqualize);
            this.tabPageOffsetFilters.Controls.Add(this.label20);
            this.tabPageOffsetFilters.Controls.Add(this.trackBarRndJitter);
            this.tabPageOffsetFilters.Controls.Add(this.label3);
            this.tabPageOffsetFilters.Controls.Add(this.btnRndJitter);
            this.tabPageOffsetFilters.Location = new System.Drawing.Point(4, 22);
            this.tabPageOffsetFilters.Name = "tabPageOffsetFilters";
            this.tabPageOffsetFilters.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOffsetFilters.Size = new System.Drawing.Size(278, 294);
            this.tabPageOffsetFilters.TabIndex = 1;
            this.tabPageOffsetFilters.Text = "Displacement filters";
            this.tabPageOffsetFilters.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(16, 187);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(98, 20);
            this.label21.TabIndex = 28;
            this.label21.Text = "Similarity thr:";
            // 
            // txtSimThreshold
            // 
            this.txtSimThreshold.Location = new System.Drawing.Point(132, 187);
            this.txtSimThreshold.Name = "txtSimThreshold";
            this.txtSimThreshold.Size = new System.Drawing.Size(127, 20);
            this.txtSimThreshold.TabIndex = 27;
            this.txtSimThreshold.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtBoxSaturation_KeyPress);
            // 
            // btnEqualize
            // 
            this.btnEqualize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEqualize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEqualize.Location = new System.Drawing.Point(176, 245);
            this.btnEqualize.Name = "btnEqualize";
            this.btnEqualize.Size = new System.Drawing.Size(83, 31);
            this.btnEqualize.TabIndex = 13;
            this.btnEqualize.Text = "Apply";
            this.btnEqualize.UseVisualStyleBackColor = true;
            this.btnEqualize.Click += new System.EventHandler(this.BtnEqualize_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(9, 140);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(269, 29);
            this.label20.TabIndex = 12;
            this.label20.Text = "Similar Zone Equalize";
            // 
            // trackBarRndJitter
            // 
            this.trackBarRndJitter.BackColor = System.Drawing.Color.White;
            this.trackBarRndJitter.Location = new System.Drawing.Point(14, 49);
            this.trackBarRndJitter.Maximum = 1000;
            this.trackBarRndJitter.Name = "trackBarRndJitter";
            this.trackBarRndJitter.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.trackBarRndJitter.Size = new System.Drawing.Size(245, 45);
            this.trackBarRndJitter.TabIndex = 11;
            this.trackBarRndJitter.TickFrequency = 5;
            this.trackBarRndJitter.Value = 20;
            this.trackBarRndJitter.Scroll += new System.EventHandler(this.TrackBarRandomJitter_Scroll);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 29);
            this.label3.TabIndex = 4;
            this.label3.Text = "Random Jitter";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel4);
            this.tabPage3.Controls.Add(this.btnStuckiDither);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.btnOrderedDither);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(278, 294);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Dithering filters";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.Location = new System.Drawing.Point(4, 77);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(270, 1);
            this.panel4.TabIndex = 16;
            // 
            // btnStuckiDither
            // 
            this.btnStuckiDither.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStuckiDither.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStuckiDither.Location = new System.Drawing.Point(181, 114);
            this.btnStuckiDither.Name = "btnStuckiDither";
            this.btnStuckiDither.Size = new System.Drawing.Size(83, 31);
            this.btnStuckiDither.TabIndex = 7;
            this.btnStuckiDither.Text = "Apply";
            this.btnStuckiDither.UseVisualStyleBackColor = true;
            this.btnStuckiDither.Click += new System.EventHandler(this.btnStuckiDither_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(14, 82);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(161, 29);
            this.label12.TabIndex = 8;
            this.label12.Text = "Stucki Dither";
            // 
            // btnOrderedDither
            // 
            this.btnOrderedDither.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrderedDither.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrderedDither.Location = new System.Drawing.Point(181, 40);
            this.btnOrderedDither.Name = "btnOrderedDither";
            this.btnOrderedDither.Size = new System.Drawing.Size(83, 31);
            this.btnOrderedDither.TabIndex = 5;
            this.btnOrderedDither.Text = "Apply";
            this.btnOrderedDither.UseVisualStyleBackColor = true;
            this.btnOrderedDither.Click += new System.EventHandler(this.BtnOrderedDither_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(14, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(188, 29);
            this.label11.TabIndex = 6;
            this.label11.Text = "Ordered Dither";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.txtSaturation);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.txtHue);
            this.tabPage4.Controls.Add(this.btnCrossDomainColorize);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.panel6);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.btnSimilarColorize);
            this.tabPage4.Controls.Add(this.panel5);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.btnSimpleColorize);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(278, 294);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Colorize filters";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(13, 189);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 20);
            this.label17.TabIndex = 28;
            this.label17.Text = "Saturation:";
            // 
            // txtSaturation
            // 
            this.txtSaturation.Location = new System.Drawing.Point(104, 189);
            this.txtSaturation.Name = "txtSaturation";
            this.txtSaturation.Size = new System.Drawing.Size(152, 20);
            this.txtSaturation.TabIndex = 27;
            this.txtSaturation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtBoxSaturation_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(13, 157);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 20);
            this.label16.TabIndex = 26;
            this.label16.Text = "New Hue:";
            // 
            // txtHue
            // 
            this.txtHue.Location = new System.Drawing.Point(104, 157);
            this.txtHue.Name = "txtHue";
            this.txtHue.Size = new System.Drawing.Size(152, 20);
            this.txtHue.TabIndex = 25;
            this.txtHue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtBoxHue_KeyPress);
            // 
            // btnCrossDomainColorize
            // 
            this.btnCrossDomainColorize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCrossDomainColorize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrossDomainColorize.Location = new System.Drawing.Point(176, 238);
            this.btnCrossDomainColorize.Name = "btnCrossDomainColorize";
            this.btnCrossDomainColorize.Size = new System.Drawing.Size(83, 31);
            this.btnCrossDomainColorize.TabIndex = 24;
            this.btnCrossDomainColorize.Text = "Apply";
            this.btnCrossDomainColorize.UseVisualStyleBackColor = true;
            this.btnCrossDomainColorize.Click += new System.EventHandler(this.BtnCrossDomainColorize_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(9, 112);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(174, 29);
            this.label15.TabIndex = 23;
            this.label15.Text = "Cross domain";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Black;
            this.panel6.Location = new System.Drawing.Point(4, 99);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(270, 1);
            this.panel6.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(9, 58);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 29);
            this.label14.TabIndex = 21;
            this.label14.Text = "Similar";
            // 
            // btnSimilarColorize
            // 
            this.btnSimilarColorize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSimilarColorize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimilarColorize.Location = new System.Drawing.Point(176, 60);
            this.btnSimilarColorize.Name = "btnSimilarColorize";
            this.btnSimilarColorize.Size = new System.Drawing.Size(83, 31);
            this.btnSimilarColorize.TabIndex = 20;
            this.btnSimilarColorize.Text = "Apply";
            this.btnSimilarColorize.UseVisualStyleBackColor = true;
            this.btnSimilarColorize.Click += new System.EventHandler(this.BtnSimilarColorize_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            this.panel5.Location = new System.Drawing.Point(4, 48);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(270, 1);
            this.panel5.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(9, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(95, 29);
            this.label13.TabIndex = 18;
            this.label13.Text = "Simple";
            // 
            // btnSimpleColorize
            // 
            this.btnSimpleColorize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSimpleColorize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSimpleColorize.Location = new System.Drawing.Point(176, 9);
            this.btnSimpleColorize.Name = "btnSimpleColorize";
            this.btnSimpleColorize.Size = new System.Drawing.Size(83, 31);
            this.btnSimpleColorize.TabIndex = 17;
            this.btnSimpleColorize.Text = "Apply";
            this.btnSimpleColorize.UseVisualStyleBackColor = true;
            this.btnSimpleColorize.Click += new System.EventHandler(this.BtnSimpleColorize_Click);
            // 
            // downsampledImagsToolStripMenuItem
            // 
            this.downsampledImagsToolStripMenuItem.Name = "downsampledImagsToolStripMenuItem";
            this.downsampledImagsToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.downsampledImagsToolStripMenuItem.Text = "Downsampled imags";
            this.downsampledImagsToolStripMenuItem.Click += new System.EventHandler(this.downsampledImagsToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.tabFilters2);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.panelImgBackground);
            this.Controls.Add(this.tabFilters1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip;
            this.MaximumSize = new System.Drawing.Size(1200, 700);
            this.MinimumSize = new System.Drawing.Size(1200, 700);
            this.Name = "MainForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image Studio";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbGammaRed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGammaGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGammaBlue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.tabFilters1.ResumeLayout(false);
            this.tabPageBasicFilters.ResumeLayout(false);
            this.tabPageBasicFilters.PerformLayout();
            this.tabPageConvFilters.ResumeLayout(false);
            this.tabPageConvFilters.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabFilters2.ResumeLayout(false);
            this.tabPageEdgeDetectFilters.ResumeLayout(false);
            this.tabPageEdgeDetectFilters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numKuwaharaSize)).EndInit();
            this.tabPageOffsetFilters.ResumeLayout(false);
            this.tabPageOffsetFilters.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRndJitter)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Panel panelImgBackground;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem originalImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem channelImagesToolStripMenuItem;
        private System.Windows.Forms.Button btnInvert;
        private System.Windows.Forms.Label lblInvert;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.TrackBar tbGammaRed;
        private System.Windows.Forms.Label lblGamma;
        private System.Windows.Forms.TrackBar tbGammaGreen;
        private System.Windows.Forms.TrackBar tbGammaBlue;
        private System.Windows.Forms.Button btnGamma;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolTip tooltip;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem safeModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unsafeModeToolStripMenuItem;
        private System.Windows.Forms.TabControl tabFilters1;
        private System.Windows.Forms.TabPage tabPageBasicFilters;
        private System.Windows.Forms.TabPage tabPageConvFilters;
        private System.Windows.Forms.TabControl tabFilters2;
        private System.Windows.Forms.TabPage tabPageEdgeDetectFilters;
        private System.Windows.Forms.TabPage tabPageOffsetFilters;
        private System.Windows.Forms.Button btnRndJitter;
        private System.Windows.Forms.Button btnEdgeDetectDiff;
        private System.Windows.Forms.Button btnEmboss;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioBtn7x7;
        private System.Windows.Forms.RadioButton radioBtn5x5;
        private System.Windows.Forms.RadioButton radioBtn3x3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem convolutionComparisonToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar trackBarRndJitter;
        private System.Windows.Forms.ToolStripMenuItem channelHistogramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grayscaleComparisonToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnGrayscaleMinMaxMean;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnGrayscaleMax;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnGrayscaleAvg;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnHistogramWindowFilter;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtRMax;
        private System.Windows.Forms.TextBox txtRMin;
        private System.Windows.Forms.TextBox txtBMax;
        private System.Windows.Forms.TextBox txtBMin;
        private System.Windows.Forms.TextBox txtGMax;
        private System.Windows.Forms.TextBox txtGMin;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnOrderedDither;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnStuckiDither;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnSimilarColorize;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnSimpleColorize;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSaturation;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtHue;
        private System.Windows.Forms.Button btnCrossDomainColorize;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown numKuwaharaSize;
        private System.Windows.Forms.Button btnKuwahara;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnEqualize;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ColorDialog clrDialog;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtSimThreshold;
        private System.Windows.Forms.ToolStripMenuItem downsampledImagsToolStripMenuItem;
    }
}

