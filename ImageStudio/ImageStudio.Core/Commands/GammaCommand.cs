﻿using System.Drawing;
using ImageStudio.Core.Filters;

namespace ImageStudio.Core.Commands
{
    /// <summary>
    /// Implements the <see cref="ICommand"/> interface for the <see cref="GammaFilter"/>.
    /// </summary>
    public class GammaCommand : ICommand
    {
        private readonly Image _image;
        private readonly bool _safeMode;
        private readonly double _redGamma;
        private readonly double _greenGamma;
        private readonly double _blueGamma;

        /// <summary>
        /// Initializes a new instance of the <see cref="GammaCommand"/> class.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> the command will be executed on.</param>
        /// <param name="safeMode">Whether the command runs in safe mode or not.</param>
        /// <param name="redGamma">The red gamma value.</param>
        /// <param name="greenGamma">The green gamma value.</param>
        /// <param name="blueGamma">The blue gamma value.</param>
        public GammaCommand(Image image, bool safeMode, double redGamma, double greenGamma, double blueGamma)
        {
            _image = image;
            _safeMode = safeMode;
            _redGamma = redGamma;
            _greenGamma = greenGamma;
            _blueGamma = blueGamma;
        }

        /// <summary>
        /// Gets the name property.
        /// </summary>
        public string Name => "Gamma";

        /// <summary>
        /// Executes the filter.
        /// </summary>
        /// <returns>The filtered image.</returns>
        public Image Execute()
        {
            return _safeMode
                ? GammaFilter.Gamma(_image, _redGamma, _greenGamma, _blueGamma)
                : GammaFilter.GammaUnsafe(_image, _redGamma, _greenGamma, _blueGamma);
        }

        /// <summary>
        /// Unexecutes the filter.
        /// </summary>
        /// <returns>The original image.</returns>
        public Image UnExecute()
        {
            return _image;
        }
    }
}
