﻿using System.Drawing;
using System.Drawing.Imaging;

namespace ImageStudio.Core.Filters
{
    /// <summary>
    /// Implements the invert filter.
    /// </summary>
    public static class InvertFilter
    {
        /// <summary>
        /// Peforms the invert filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <returns>The filtered image.</returns>
        public static Image Invert(Image image)
        {
            using (var bmp = new Bitmap(image))
            {
                for (var i = 0; i < bmp.Width; i++)
                {
                    for (var j = 0; j < bmp.Height; j++)
                    {
                        var c = bmp.GetPixel(i, j);
                        bmp.SetPixel(i, j, Color.FromArgb(255 - c.R, 255 - c.G, 255 - c.B));
                    }
                }

                return new Bitmap(bmp);
            }
        }

        /// <summary>
        /// Peforms the invert filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> that is being filtered.</param>
        /// <returns>The filtered image.</returns>
        public static Image InvertUnsafe(Image image)
        {
            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                p[0] = (byte)(255 - p[0]);
                                p[1] = (byte)(255 - p[1]);
                                p[2] = (byte)(255 - p[2]);

                                p+=3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }

                return new Bitmap(bmp);
            }
        }
    }
}
