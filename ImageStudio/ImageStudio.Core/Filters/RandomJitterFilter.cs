﻿using System;
using System.Drawing;

namespace ImageStudio.Core.Filters
{
    /// <summary>
    /// Implements the random jitter filter.
    /// </summary>
    public static class RandomJitterFilter
    {
        /// <summary>
        /// Performs the random jitter filter operation on the specified <see cref="Image"/>.
        /// </summary>
        /// <param name="image">The <see cref="Image"/> being filtered.</param>
        /// <param name="degree">The degree parameter.</param>
        /// <returns>The filtered image.</returns>
        public static Image RandomJitter(Image image, int degree = 5)
        {
            CheckValue(degree);

            var points = new Point[image.Width, image.Height];

            var width = image.Width;
            var height = image.Height;

            int newX, newY;

            var half = (short)Math.Floor((double) degree / 2);
            var rnd = new Random();

            for (var x = 0; x < width; ++x)
            {
                for (var y = 0; y < height; ++y)
                {
                    newX = rnd.Next(degree) - half;

                    points[x, y].X = CalculateValue(x, newX, width);

                    newY = rnd.Next(degree) - half;

                    points[x, y].Y = CalculateValue(y, newY, height);
                }
            }

            var result = OffsetFilter.Offset(image, points);

            return result;
        }

        /// <summary>
        /// Calculates the value based on the specified parameters.
        /// </summary>
        /// <param name="baseValue">The base value.</param>
        /// <param name="offsetValue">The offset value.</param>
        /// <param name="upperBound">The upper bound for the value.</param>
        /// <returns>The calculated value.</returns>
        private static int CalculateValue(int baseValue, int offsetValue, int upperBound)
        {
            var value = baseValue + offsetValue;

            var result = value > 0 && value < upperBound ? offsetValue : 0;

            return result;
        }

        /// <summary>
        /// Checks if the value is within the valid range.
        /// </summary>
        /// <param name="value">The value being checked.</param>
        private static void CheckValue(int value)
        {
            if (value < 0 || value > short.MaxValue)
            {
                throw new ArgumentOutOfRangeException(nameof(value), "Value is out of range for short type.");
            }
        }
    }
}
