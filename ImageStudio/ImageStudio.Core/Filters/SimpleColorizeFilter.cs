﻿using System.Drawing;
using System.Drawing.Imaging;

namespace ImageStudio.Core.Filters
{
    public static class SimpleColorizeFilter
    {
        private static readonly Color[] GrayscalePaletteArray = new Color[256];

        public static Image Colorize(Image image, Image similarImage = null)
        {
            if (similarImage != null)
            {
                FillGrayscalePaletteArray(similarImage);
            }

            var colorsArray = similarImage == null ? FillGrayscalePaletteArray() : GrayscalePaletteArray;

            using (var bmp = new Bitmap(image))
            {
                for (var x = 0; x < image.Width; x++)
                {
                    for (var y = 0; y < image.Height; y++)
                    {
                        var color = bmp.GetPixel(x, y);
                        var grayVal = colorsArray[color.R];
                        bmp.SetPixel(x, y, grayVal);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        public static Image ColorizeUnsafe(Image image, Image similarImage = null)
        {
            if (similarImage != null)
            {
                FillGrayscalePaletteArray(similarImage);
            }

            var colorsArray = similarImage == null ? FillGrayscalePaletteArray() : GrayscalePaletteArray;

            using (var bmp = new Bitmap(image))
            {
                var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmData.Stride;
                var scan0 = bmData.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;

                        var offset = stride - bmp.Width * 3;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                var newColor = colorsArray[p[2]];

                                p[2] = newColor.R;
                                p[1] = newColor.G;
                                p[0] = newColor.B;

                                p += 3;
                            }
                            p += offset;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmData);
                    }
                }

                return new Bitmap(bmp);
            }
        }

        private static void SetGrayPaletteArrayVal(int i, Color value)
        {
            if (GrayscalePaletteArray[i] == Color.Empty)
            {
                GrayscalePaletteArray[i] = value;
            }
        }

        private static Color[] FillGrayscalePaletteArray()
        {
            var newColors = new Color[256];
            int r = 127, g = 127, b = 127;

            const int step = 36;

            for (var i = 0; i < 256; i++)
            {
                if (i <= step)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    r += 3;
                }
                else if (i <= step * 2)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    g += 3;
                }
                else if (i <= step * 3)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    r -= 3;
                }
                else if (i <= step * 4)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    b += 3;
                }
                else if (i <= step * 5)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    g -= 3;
                }
                else if (i <= step * 6)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    r += 3;
                }
                else if (i <= step * 7)
                {
                    newColors[i] = Color.FromArgb(r, g, b);
                    g += 3;
                }
                if (r < 0)
                {
                    r = 0;
                }
                if (g < 0)
                {
                    g = 0;
                }
                if (r > 255)
                {
                    r = 255;
                }
                if (g > 255)
                {
                    g = 255;
                }
                if (b > 255)
                {
                    b = 255;
                }
            }

            return newColors;
        }

        private static void FillGrayscalePaletteArray(Image img)
        {
            using (var bmp = new Bitmap(GrayscaleFilter.GrayscaleAvgUnsafe(img)))
            using (var bmpOrig = new Bitmap(img))
            {
                var bmDataOrig = bmpOrig.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                var bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

                var stride = bmpData.Stride;
                var scan0 = bmpData.Scan0;
                var strideOrig = bmDataOrig.Stride;
                var scan0Orig = bmDataOrig.Scan0;

                unsafe
                {
                    try
                    {
                        var p = (byte*)(void*)scan0;
                        var pOrig = (byte*)(void*)scan0Orig;

                        var offset = stride - bmp.Width * 3;
                        var offsetOrig = strideOrig - bmpOrig.Width * 3;

                        for (var y = 0; y < bmp.Height; ++y)
                        {
                            for (var x = 0; x < bmp.Width; ++x)
                            {
                                SetGrayPaletteArrayVal(p[0], Color.FromArgb(pOrig[2], pOrig[1], pOrig[0]));

                                p += 3;
                                pOrig += 3;
                            }
                            p += offset;
                            pOrig += offsetOrig;
                        }
                    }
                    finally
                    {
                        bmp.UnlockBits(bmpData);
                        bmpOrig.UnlockBits(bmDataOrig);
                    }
                }
            }
        }
    }
}
